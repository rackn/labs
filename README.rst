.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Labs

.. _rs_labs_toc:

Digital Rebar Platform Labs
---------------------------

Labs are interactive, guided trainings that provide a hands-on approach to learning about DRP and its systems.

The repository of base RackN-provided labs is available `on GitLab <https://gitlab.com/rackn/labs>`_.

Available labs
~~~~~~~~~~~~~~

Please consult the following lab resources:

.. toctree::
   :maxdepth: 1
   :glob:

   labs/*

Style Guide
~~~~~~~~~~~

In order to help with consistency and flow, we suggest that the following guidelines be followed when making and setting up labs.

General Comments
================

* Tell a story through the 'Guided Lab Modal' dialog box
* Try not to retell things from other labs; Prereqs are your friend

Most fields can reference Markdown.  There are two sets of markdown, inline markdown and markdown.  In general, inline markdown
is supposed to render as a single line of styled text with links. `Markdown <https://www.markdownguide.org/cheat-sheet>`_ can be any markdown feature.

There is a special helper that the UX and Doc renderers will translate into special links for interacting with the UX.  If a URL
use `ux://path`, this will be translated into a reference to the currently attached DRP endpoint with that path.  This is use for
creating links to sections of the UX.

Here are some examples:

* ux://cluster/Name:lab1010 - A link to the editor of the `lab1010` cluster.
* ux://overview - A link to the overview page
* ux://machines - A link to the machines page.
* ux://resource_brokers/Name:guacd-service/activity - A link to the specific Resource Broker editor, `guacd-service`, opened to the `Activity` tab.

When using code-blocks, please provide the type of code when possible.  Something like the following in markdown:

::

  ```sh
     my_bash_code.sh
  ```

Some thought should be given with regard to markdown.  The labs are converted into RST for the RackN docs.  This is usually not a problem, but
sometimes additional lines are needed to make sure lists are rendered correctly.  A List should have an empty line before it.

Suggestions
===========

Some phrasing suggestions:

+----------------+-----------------+
| Avoid          | Use             |
+================+=================+
| Go to XXX View | Navigate to XXX |
+----------------+-----------------+
| UX*            | Portal          |
+----------------+-----------------+

UX is strange.  In general, we want to talk about the Portal and going to the Portal,
but some cases we describing add a feature to the Portal.   This is still a fuzzy area.

When talking about how to fill out a form for submissions, use the Objective
to open the form and the Description to fill out the form.  This makes for 
tigheter and easier to follow sections.  Remember to add how to get out of
the form.


Lab Base Structure
==================

All labs should start with the following line.  This allows tools like VSCode to automatically format and validated the lab.

.. code-block:: yaml

    # yaml-language-server: $schema=../../schema.json


The format is defined in the `schema.json` file in the labs tree.  The top level data of a Lab is as follows:

* Id - REQUIRED string identifier for a lab.  This is in general a number by convention.
* Type - REQUIRED string and must be *labs*
*	Enabled - Boolean value indicating if the lab should be included in view.  Defaults to `false`.
* Name - Name of the Lab and used as the title in the views and documentation.  This can be inline markdown.
* Objective - This describes the high level goal of the lab.  Usuaully short and can be inline markdown.
* Description - Optional extended text that expands upon the Objective.  This can be markdown.
* Time - Integer number of minutes that the lab will take to complete.
* Concepts - List of String values that represent concepts in RackN DRP that are covered by the lab.
* Tags - List of String values that represent interesting tags for sorting and searching.
* Difficulty - The hardness of the lab - This can be one of:

   * introductory
   * intermediate
   * advanced

* VideoUrl - An Optional URL to the YouTube video content of the lab.  Must be YouTube video.
* Prereqs - A structure that defines what is required to run this lab.

   * Labs - A list of string Ids of labs that should be completed before this one.
   * Checklist - A list of Objects with a `Label` and `Description` field that describe something
     that needs to be done first.  These can be inline markdown and markdown, respectively.

* LabSection - This is a single Section that represents the steps to do the lab.  See below.

*Tags* and *Concepts* strings should be lower case hyphenated words for consistency.

Lab Sections
============

The base lab has a single section that must follow the following rules.  Sections can contain other sections.

Sections have the following fields:

*	Name - Optional name of the section.  This is often used as a sub-header. Can be inline markdown.
* Objective - Optional action the user should take.  Objectives are numbered entries for their level in the sections.  Can be inline markdown.
* Description - Optional additional details about the Section.  Can be markdown.
* Tabbed - Optional string indicating that the children of this section are treated as a tabbed set.  The Names of those sections are the tab names.
  The string used in this field is synchornized across the labs and a choice made in one section will be reflected in other sections with a similar
  name.
* Sections - A list of additional sections.  These will be nested under the current section and processed recursively.

Generally these are true, but exceptions could apply.  Try to stick with them.

* A section should have a Name or an Objective, but not both.
* Objectives are used to indicate actions the user should take.
* Names are used to make Headers and/or Grouping.
* Names should not contain actions that the user is expected to do.  Make that an Objective so that it gets a numbered element.
* Tabbed sections are great ways to generate separate paths through the lab with choice points.  Try to reuse names for common groupings.
  For example:

    * OS choices are tagged with `os` - Linux, Windows, Darwin
    * Cloud Choices are tagged with `installsh` - AWS, Google, Azure, Digital Ocean, General, Other Clouds.

Punctuation
===========

With regard to phrasing and sentences, each element type has a recommended style to follow.

* Name - A short descriptive phrase for the lab or the section.  It should not have terminating punctation as it is not a sentence. Only inline markdown.
* Objective - A short declarative phrase in imperative voice.  It should not have terminating punctation as it is not a sentence. Only inline markdown.
* Description - This is a multi-line text field with full markdown support.  Sentences are the expected form to describe the Section (`Name`) or `Objective`.
  This can include additional markdown structures such as lists and tables.  Lists should follow the similar structure as a `Name` or an `Objective` depending
  upon what the list is describing.  Tables are a mix based upon columns and usage.


Creating your own labs
~~~~~~~~~~~~~~~~~~~~~~

A DRP endpoint can be configured to fetch lab content packs from multiple URLs. For reference, the base
RackN labs are available at `labs <https://rebar-catalog.s3.us-west-2.amazonaws.com/labs/latest.json>`_.

It is very simple to set up your own environment to create and test your own labs.

Setting up the environment
==========================

We recommend `Visual Studio Code <https://code.visualstudio.com>`_ for editing lab content packs. The repository
has configured YAML styling and a specific YAML extension that plays with our JSON Schema for editing labs.

First, you'll want to clone the labs repository and open it in your editor of choice.

.. code-block:: shell

    $ git clone https://gitlab.com/rackn/labs

At this point, if you're using Visual Studio Code, ensure the recommended extensions are installed. It should
show as a notification in the editor.

Lab content packs are folders at the top-level with a `meta.yaml` file directly inside them. For example, the
`labs` folder is the base RackN-provided lab content pack.

* If you choose to **edit the base labs**, you can do so simply by editing the files in its `labs` directory.
* If you choose to **create your own content pack**, you can create a new folder alongside `labs` with
  a similar `meta.yaml` file (using the `normal content pack meta object <https://docs.rackn.io/stable/arch/models/content/?h=content+pack#metadata>`_).
  Optionally, you can delete the `labs` directory to prevent the content pack packager from bundling that
  content pack as well.

When creating your own labs `meta.yaml`, please suffix your content pack name with `-labs`, e.g. `cloud-labs`.

Adding labs
===========

Once you have your content pack folder (`labs` or other) with a valid `meta.yaml` file in it, you can start
adding labs by adding a new directory `labs` inside this content pack folder.

Labs are `.yml`/`.yaml` files with the format `XXXX-lab-name-lab.yml`, where `XXXX` is a 4-digit lab ID
(like 1000, 1010, 2050, etc.) and `lab-name` is a shortened version of the lab's name.

To use our JSON Schema for autocompletion, ensure the following line is at the top of your lab's YAML file.
This informs the YAML extension that a JSON Schema is available.

.. code-block:: yaml

    # yaml-language-server: $schema=../../schema.json

Refer to other existing lab files for reference.

Lab prerequisites are expected to be namespaced lab IDs. For example, if your lab depends on base lab 1000,
then the prerequisite lab should be `rackn/1000`. The namespace is derived from the content pack name, minus
its `-labs` suffix. If you would like to use a content pack `cloud-labs`'s lab 3040, use `cloud/3040`.

Packaging
=========

Labs can be packaged using the bundled package script.

* If `Go <https://golang.org/>`_ is installed, the script will compile `drpcli <https://docs.rackn.io/stable/operators/cli/?h=drpcli>`_
  and use it locally.
* Alternatively, skip the compilation step and the Go requirement by creating a `bin` folder in the repo
  directory and adding the `drpcli` binary directly. It can be downloaded from your existing DRP server
  at `https://[drp server address]:8092/files/drpcli.amd64.linux`.

When you would like to package your content pack, run the following from the repo directory.

.. code-block:: shell

    $ ./tools/package.sh

This will create several files at the root of the repo directory. Most importantly is `pack-folder-name.json`,
where `pack-folder-name` is the name of your content pack's folder. For example, the base labs would package to
`labs.json`. From here on out, this file will be referred to as `labs.json`.

For iterative testing, it is recommended that you set up a tool to watch `**/labs/*.yml` for changes, and
consequently run `./tools/publish.sh` on every change. If you are using Visual Studio Code, the extension
`Run On Save <https://marketplace.visualstudio.com/items?itemName=emeraldwalk.RunOnSave>`_ has been preconfigured
to do this for you.

Adding your content pack to your DRP
====================================

The packaged `labs.json` file can only be accessed from the RackN Portal in the following conditions:

* Served via HTTPS
* The CORS header is present: `Access-Control-Allow-Origin: *`

This repo provides a tool for statically serving this for development purposes.

To setup this tool, you need to install node: `Install node via nvm <https://github.com/nvm-sh/nvm#installing-and-updating>`_.

.. code-block:: shell

    $ nvm use 19  # or latest version!
    $ nvm alias default 19

To run this tools:

.. code-block:: shell

    $ ./tools/serve.sh

To make this tool build the labs.json file at time of request, run with `BUILD=true` (instead of using editor tooling to build on save)

.. code-block:: shell

    $ BUILD=true ./tools/serve.sh

The `labs.json` file should be available at `https://127.0.0.1:9000/labs.json`.

Copy this URL and head over to your DRP in the `UX <https://portal.rackn.io/>`_

Access the `global profile <https://portal.rackn.io/#/e/0.0.0.0/profiles/global>`_ from the Profiles view.

Add an array param to the global profile named `lab_urls`.

.. image:: images/lab_urls_param.png
    :align: center
    :alt: Adding the param to the global profile

When this param is an empty array, the Labs button on the UX header will disappear as there are no lab URLs to fetch from.

You can add the base labs URL from above to the array like such:

.. code-block:: json

    ["https://rebar-catalog.s3.us-west-2.amazonaws.com/labs/latest.json"]

Then, you can add also your served `labs.json` URL to this array to add your labs to your DRP.

.. code-block:: json

    [
        "https://127.0.0.1:9000/labs.json#dev", // this URL is not going to be the same for you!
        "https://rebar-catalog.s3.us-west-2.amazonaws.com/labs/latest.json"
    ]

The `#dev` suffix to your URL indicates to the UX that labs coming from that content pack will periodically refresh,
allowing for quick iteration in testing labs.

.. note::
  URLs that appear earlier in the `lab_urls` param will take precedence over URLs that appear later.

Add Server Cert to Firefox
==========================

Go to Settings -> Privacy and Security -> Security -> Certs -> Veiw Certificates. Open the "Servers" tab.
"Add Exception" and input *ip*:9000 of the server you are hosting the lab content from.

Serve Labs on Specific/All IPs
==============================

By default NodeJS serves on `127.0.0.1:9000`, if you need to access the labs remotely you can do so by setting the `HOST` variable when running `tools/serve.sh`

To listen on all interfaces:

.. code-block:: shell

    $ HOST='0.0.0.0' ./tools/serve.sh
    
To listen on a specific interface:

.. code-block:: shell

    $ HOST='192.168.1.100' ./tools/serve.sh

To listen on all interfaces and build the labs.json:

.. code-block:: shell

    $ HOST='0.0.0.0' BUILD=true ./tools/serve.sh

Viewing your labs
=================

Once you have saved the `lab_urls` param on the global profile, click the green Labs button from the top of
the UX, and you should see a node graph of all labs provided by the content packs at your `lab_urls`.

Congratulations! You have created your first lab content pack.

You can remove the `lab_urls` param from the global profile at any time to return to only the base RackN labs.


.. Release v4.11.0 Start

.. Release v4.13.0 Start
