const https = require("https");
const fs = require("fs");
const path = require("path");
const execFileSync = require("child_process").execFileSync

const options = {
  key: fs.readFileSync(path.join(__dirname, "cert/key.pem")),
  cert: fs.readFileSync(path.join(__dirname, "cert/cert.pem")),
};

const ROOT =
  path.resolve(".") === __dirname ? path.resolve("../..") : path.resolve(".");
const PORT = Number(process.env.PORT || 9000);

https
  .createServer(options, (req, res) => {
    const file = path.join(ROOT, req.url);
    const headers = {
      // Allow the UX to access this server
      "access-control-allow-origin": "*",
      // force the browser to not cache the content of the served files
      "cache-control": "no-cache",
    };

    console.log(req.method, file.replace(ROOT, ""));

    // options sends cors headers
    if (req.method === "OPTIONS") {
      res.writeHead(200, headers);
      res.end("ok");
      return;
    }

    // we only support GET
    if (req.method !== "GET") {
      res.writeHead(404, headers);
      res.end("not found");
    }

    // don't allow reading all files
    if (!file.startsWith(ROOT)) {
      res.writeHead(401, headers);
      res.end("unauthorized");
      return;
    }

    if (process.env.BUILD && file.endsWith('.json')) {
      try {
        console.info('BUILD tools/package.sh', execFileSync('tools/package.sh', [], {
          cwd: path.resolve(process.cwd(), '../..'),
        }).toString())
      } catch (err) {
        console.error('error running tools/package.sh:', err);
      }
    }

    try {
      if (!fs.existsSync(file)) {
        res.writeHead(404, headers);
        res.end('not found');
        return
      }

      if (fs.lstatSync(file).isDirectory()) {
        res.writeHead(200, { ...headers, "content-type": "text/html" });
        const files = fs.readdirSync(file);
        res.end(
          files
            .map(
              (f) =>
                `<a href="${path
                  .join(file, f)
                  .replace(ROOT, "")}">${f}</a><br/>`
            )
            .join("\n")
        );
        return;
      }

      // read static files
      const data = fs.readFileSync(file, "utf-8");
      res.writeHead(200, headers);
      res.end(data);
    } catch (err) {
      res.writeHead(500, headers);
      res.end(JSON.stringify(err));
      return;
    }
  })
  .listen(PORT, () => {
    console.log("Listening on " + PORT + "...");
  });
