#!/bin/bash

set -e

family=386
if [[ $(uname -m) == x86_64 ]] ; then
    family=amd64
fi
case $(uname -s) in
    Darwin)
        shasum="command shasum -a 256"
        ;;
    Linux)
        shasum="command sha256sum"
        ;;
    *)
        # Someday, support installing on Windows.  Service creation could be tricky.
        echo "No idea how to check sha256sums"
        exit 1;;
esac

P=`pwd`
PATH="$P/bin:$PATH"
mkdir -p bin
export GO111MODULE=on
[[ -x $P/bin/drpcli ]] || (go mod tidy ; go build -o $P/bin/drpcli tools/drpcli.go)

version=$(tools/version.sh)

tools/pieces.sh | while read i ; do
    dir=$i
    echo "Building bundle: $i"
    ldir=$(pwd)
    if [ -d $dir/content ] ; then
        cd $dir/content
        rm -f ._Version.meta
        drpcli contents bundle $ldir/$i.yaml Version=$version
        drpcli contents bundle $ldir/$i.json Version=$version
        cd -
    else
        cd $dir
        rm -f ._Version.meta
        drpcli contents bundle $ldir/$i.yaml Version=$version
        drpcli contents bundle $ldir/$i.json Version=$version
        cd -
    fi

    echo "Building documents: $i"
    drpcli contents document $i.yaml > $i.rst
    $shasum $i.yaml > $i.sha256
done

