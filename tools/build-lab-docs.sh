#!/bin/bash

# This is used to build the labs docs.
# Run this after tools/build.sh

# This requires pandoc and sphinx tools installed.

rm -rf docs/labs
mkdir -p docs/labs

for i in `ls labs/labs/*.yml`
do
        f=$(echo $i | sed 's/.yml/.rst/')
        f=$(basename $f)
        echo "Building doc for $i"
        drpcli labs document doc.tmpl $i > docs/labs/$f
done

rm -rf tmpfile.input tmpfile.output
rm -f docs/labs/0000-sampleLab-lab.rst
