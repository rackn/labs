# yaml-language-server: $schema=../../schema.json
Name: Create an Alert when Content is updated
Id: '1050'
Type: labs
Enabled: true
VideoUrl: https://youtu.be/-9Xl8fI_6MI
Objective: Be alerted when content is updated in a Digital Rebar endpoint
Description: |+
  **Business ROI**: Operators must be able to track and verify that environments have only been updated by planned processes
Time: 10
Difficulty: introductory
Prereqs:
  Labs:
    - '1005'
LabSection:
  Name: Summary
  Sections:
    - Name: Inspect your system
      Sections:
        - Objective:
            Navigate to [System Information and Preferences](ux://system) for
            the list of API Objects at the bottom right
        - Objective:
            Navigate to [Machines](ux://machines) to find the Digital Rebar
            Endpoint Self-Runner
          Description:
            The name will match the Digital Rebar Endpoint ID in the top left
            corner.
        - Objective: Open The Live Event Panel
          Description:
            The Live Event Panel can be opened by clicking the megaphone icon on
            top of the side nav, next to the Digital Rebar Endpoint ID.
        - Objective:
            Navigate to [Catalog](ux://catalog) and update or install the
            ["dev-library"](ux://catalog/dev-library) Content Pack or Plugin
            from the library
          Description: |+
            Observe the "CREATE contents dev-library" event in the Live event Panel
        - Objective:
            Navigate to [Triggers](ux://triggers) page and create a new Trigger
          Description: |+
            Name: lab1050

            Provider: `event-trigger`

            Blueprint: `alerts-on-content-change`

            Filter to the Local Self Runner

            Param `event-trigger/event-match` should be set to `contents.*.*`

            Save

            Set MergeData: `true`
        - Objective:
            Navigate to [Catalog](ux://catalog) and update or install a Content
            Pack from the library
          Description:
            The specific Content Pack installed does not matter as the action of
            installing one will run the Trigger configured in the previous
            steps.
        - Objective:
            Navigate to [Alerts](ux://alerts) to review the created Alert(s)
          Description:
            Acknowledge the alerts(s) by clicking on the bell icon in the
            leftmost column or clicking into the alert and clicking "Acknowledge
            Alert" on the top right.
    - Name:
        Return to the [Triggers](ux://triggers) page to elevate the alert level
      Sections:
        - Objective: Open the lab1050 trigger
        - Objective:
            In the `Work Order Config` params list, add the `alert/level`
            Parameter with value `WARN`
          Description:
            This is done in the "Work Order Config" params  at the bottom of the
            Editor tab instead of the "Params" tab as the Params tab is for
            Trigger specific params.
        - Objective:
            Repeat the [Catalog](ux://catalog) update process and note that the
            [Alerts](ux://alerts) are now WARN level
Tags:
  - alerts
  - triggers
  - compliance
  - orchestration
Concepts:
  - triggers
  - alerts
  - content packs
