# yaml-language-server: $schema=../../schema.json
Name: Install Digital Rebar
Id: '1000'
Type: labs
Enabled: true
VideoUrl: https://youtu.be/MqUl_Jxuv1Q
Objective: Install your own Digital Rebar test server
Description: |+
  **Business ROI** Unlike multi-tenant SaaS, Digital Rebar is user operated software: you’ll never have to open your firewall for us, share sensitive information or upload access credentials.
Time: 15
Difficulty: introductory
LabSection:
  Name: Install Digital Rebar
  Sections:
    - Name: Provision a Machine
      Description: |+
        Acquire a machine from your Virtual Machine Manager or Cloud Provider
        console.
      Tabbed: installs
      Sections:
        - Name: Cloud or VM via Console
          Description:
            These steps are also found in the [cloud
            install](https://docs.rackn.io/stable/getting-started/install/install-cloud/)
            guide.
          Sections:
            - Objective:
                Instantiate a Linux machine with a recent version (most major
                variants are supported)
            - Objective: Have access to the Internet
              Description:
                No access? see [Air Gap
                install](https://docs.rackn.io/stable/getting-started/install/install-airgap/)
            - Objective:
                Provide at least 2 CPUs and 8 GB of RAM for a lab system
            - Objective:
                Make sure it has inbound access to SSH (22) and ports 8090-8092
              Description: |-
                For example, use
                ```sh
                firewall-cmd --permanent --add-port=22/tcp --add-port=8090-8093/tcp --add-port=67-69/udp --add-port=4011/udp --add-port=8080/tcp --add-port=53/tcp --add-port=53/udp
                ```

                See [port details](https://docs.rackn.io/stable/arch/endpoint/server/#rs_arch_ports) in the architecture docs for more information on all possible ports.
            - Objective: Note the public IP address of the new instance
            - Objective: SSH to your new cloud instance
        - Name: Terraform
          Description: |
            Cloud via Terraform uses steps in the [online Cloud
            install](https://docs.rackn.io/stable/getting-started/install/install-cloud/)
            guide

            Access to run [terraform](https://www.terraform.io/downloads) with working AWS [credential and config](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) files are needed to successfully complete this lab.
          Sections:
            - Objective: |
                Download the cloud appropriate Terraform plan
                [AWS Cloud](https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/aws.tf?inline=false),
                [Google](https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/google.tf?inline=false),
                [Azure](https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/azure.tf?inline=false),
                [Digital Ocean](https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/digitalocean.tf?inline=false), or
                [Linode](https://gitlab.com/rackn/provision/-/raw/v4/integrations/terraform/linode.tf?inline=false)
            - Objective: Run the Terraform plan
              Description: |+
                ```sh
                terraform init
                terraform apply \
                  -var="drp_username=rackn" -var="drp_password=myP4ssword"
                ```
                The plan defaults to using your public key located at `~/.ssh/id_rsa.pub`.  You can specify an alternate public key file by appending `-var="ssh_pub_key=/path/to/key.pub"` to the `terraform apply` command.
            - Objective: Wait for Terraform to provide the DRP Server URL
            - Objective: Skip to the **Login and Register** step
        - Name: PXE (Bare Metal and Local)
          Description: |
            The [Quick Start](https://docs.rackn.io/stable/getting-started/) covers local Virtual Machine Manager or bare metal server.
          Sections:
            - Objective:
                Instantiate a Linux machine with a recent version (most major
                variants are supported)
            - Objective: Have network with access to the Internet
              Description:
                No access? see [Air Gap
                install](https://docs.rackn.io/stable/getting-started/install/install-airgap/)
            - Objective: Have a DHCP isolated network
              Description:
                For example in Virtualbox, provide a host only network with DHCP
                disabled.
            - Objective:
                Provide at least 2 CPUs and 8 GB of RAM for a lab system
            - Objective:
                Make sure it has inbound access to SSH (22) and ports 8090-8092
              Description: |-
                For example, use
                ```sh
                firewall-cmd --permanent --add-port=22/tcp --add-port=8090-8092/tcp && firewall-cmd --reload
                firewall-cmd --add-port=22/tcp --add-port=8090-8092/tcp && firewall-cmd --reload
                ```
            - Objective: Note the access IP address of the new instance
            - Objective: Console or SSH to your new machine
    - Name: Install Digital Rebar
      Sections:
        - Name: Choose the an install method
          Tabbed: installsh
          Sections:
            - Name: General
              Sections:
                - Objective: Run the following
                  Description: |+
                    ```sh
                    curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal
                    ```
            - Name: AWS
              Sections:
                - Objective:
                    Run the following or pass it into the User Data during
                    instance create
                  Description: |+
                    ```sh
                    value=$(curl -sfL http://169.254.169.254/latest/meta-data/public-ipv4)
                    # install Digital Rebar
                    curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
                    ```
            - Name: Google
              Sections:
                - Objective:
                    Run the following or pass it into the User Data during
                    instance create
                  Description: |+
                    ```sh
                    value=$(curl -sfL -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)
                    # install Digital Rebar
                    curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
                    ```
            - Name: Azure
              Sections:
                - Objective:
                    Run the following or pass it into the User Data during
                    instance create
                  Description: |+
                    ```sh
                    value=$(curl -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/publicIpAddress?api-version=2021-05-01&format=text")
                    # install Digital Rebar
                    curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
                    ```
            - Name: Digital Ocean
              Sections:
                - Objective:
                    Run the following or pass it into the User Data during
                    instance create
                  Description: |+
                    ```sh
                    value=$(curl -sfL "http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address")
                    # install Digital Rebar
                    curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
                    ```
            - Name: Linode
              Sections:
                - Objective: Run the following
                  Description: |+
                    ```sh
                    curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal
                    ```
            - Name: Other Clouds
              Sections:
                - Objective:
                    Substitute the [public|access IP address] and run the
                    following
                  Description: |+
                    ```sh
                    value=[public|access IP address]
                    curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --ipaddr=$value
                    ```
        - Name: Alternative options
          Description: |+
            Common additional install flags include

            |Argument |Description |
            :--- | :---
            | `--version=tip`         | Sets the install version to the latest instead of the current stable                |
            | `--drp-user=[user]`     | Adds a new super user to the endpoint                                               |
            | `--drp-password=[pass]` | Sets the password for the current user (rocketskates) or the new user if specified. |
            | `--remove-rocketskates` | Removes the current rocketskates user if different than *--drp-user*                |
            | `--drp-id=replaceme`    | Sets the ID of the DRP Endpoint at install time                                     |

    - Name: Login and Register
      Description:
        You can login when you see the **Digital Rebar version** after `Waiting
        for dr-provision to start` completes
      Sections:
        - Objective: Visit your server's API url
          Description: |+
            ```https://[public|access IP address]:8092```
        - Objective: Accept the unsigned certificate
          Description:
            This must be done initially until you install a signed certificate.
        - Objective: Login using the system username and password
          Description: Default is `rocketskates` and `r0cketsk8ts`
        - Objective: Complete the license form to create your license
          Description: Or, upload your license if you've already created one.
    - Name: Explore your system
      Description:
        We have a library of interactive labs to continue your journey!
      Sections:
        - Objective:
            Visit [Info and Preferences](ux://system) page to review the System
            Bootstrapping Wizard
          Description:
            If you are using the default password, follow the "Change Default
            Password" link in the Wizard
        - Objective:
            On the [Machines](ux://machines) page, observe the self-runner
            machine that was created during the installation to process
          Description:
            If you have loggged in before installation completes, you can
            observe Digital Rebar completing the bootstrapping process.
            workflows for your Digital Rebar service.
        - Objective:
            On the [Jobs](ux://jobs) page, observe the jobs that were run during
            the universal bootstrap process to setup the machine
        - Objective:
            On the [Catalog](ux://catalog) page, check out the versions of all
            the components in use by Digital Rebar
          Description:
            If you used `stable` to install, you can upgrade to `tip` for the
            latest code from this page.
        - Objective:
            Visit the [License Manager](ux://license) page, to extend your
            no-obligation trial license for a 30 days
Tags:
  - core
Concepts:
  - install
