# yaml-language-server: $schema=../../schema.json
Name: Start Remote Access
Id: '1200'
Type: labs
Enabled: true
Objective: Start Guacd Service to Allow Remote SSH Access to Systems
Description: |+
  **Business ROI**
Time: 5
Difficulty: introductory
Prereqs:
  Labs:
    - '1005'
  Checklist:
    - Label:
        Ensure an SSH server is running on the DRP endpoint (required for
        testing)
    - Label:
        Ensure the SSH port (22) is open for in-bound access (required for
        testing)
LabSection:
  Name: Remote Access
  Sections:
    - Name: Setup
      Sections:
        - Name: Navigate to the Machine for the DRP Endpoint in the Portal
          Sections:
            - Objective: Navigate to the [Machines](ux://machines) view
            - Objective:
                Open the editor for the DRP endpoint by clicking the name of the
                endpoint
              Description:
                The name will match the Digital Rebar Endpoint ID in the top
                left corner.
        - Name: Make sure the DRP Endpoint Machine is in unlocked
          Sections:
            - Objective: Click the *Unlock Object* Button on the top right
              Description: |-
                If *Lock Object* is present, then the machine is
                already unlocked.

                When a machine is locked, it is in a Read Only state and changes
                cannot be made.
        - Name: Add the `bootstrap-guacd` Profile to the DRP Endpoint's machine
          Sections:
            - Objective: Click the `+` icon by the *Profiles* line
              Description:
                Skip to the next step if `bootstrap-guacd` is already in the
                list.
            - Objective: Type and select `bootstrap-guacd`
        - Name: Re-run the bootstrap process
          Sections:
            - Objective: Select the `Activity` Tab
            - Objective:
                Click the `Select Blueprint` button and select by typing
                `rebootstrap-drp`
            - Objective: Click `Apply`
        - Name: Wait for the bootstrap process to finish.
          Sections:
            - Objective:
                Watch the `Activity` Tab for the *WorkOrder* to complete
            - Description: The DRP endpoint machine should finish all tasks.
    - Name: Validation
      Sections:
        - Name: Validate `guacd-service` is running
          Sections:
            - Objective:
                Navigate to the [Resource Brokers view](ux://resource_brokers)
                to the
                [`guacd-service`](ux://resource_brokers/Name:guacd-service/activity)
            - Objective: There should be a running and pending Work Order.
        - Name: Connect to the DRP Endpoint with SSH
          Sections:
            - Objective: Navigate to the [Machines](ux://machines) table
            - Objective:
                Open the editor for the DRP endpoint by clicking the name of the
                endpoint
            - Objective: Click the `Remote` button in upper right
              Description:
                An active SSH session should start from within the Portal
    - Name: Alternative Setup
      Sections:
        - Objective:
            Add `--initial-profiles=bootstrap-guacd` to the `install.sh` line in
            Lab 1000 (Install Digital Rebar)
          Description:
            The Remote Access System can be configured at install time

Tags:
  - ssh
  - remote
Concepts:
  - machines
