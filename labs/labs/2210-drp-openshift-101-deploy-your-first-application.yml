# yaml-language-server: $schema=../../schema.json
Name: "DRP OpenShift 101: Deploy Your First Application"
Id: '2210'
Type: labs
Enabled: true
Objective: "Deploy and expose your first application on OpenShift"
Description: |
  In this lab, you'll learn how to deploy a basic web application on your OpenShift cluster.
  You'll use OpenShift's build and deployment capabilities to containerize and run the 
  application, then expose it using a route.

Time: 20
Difficulty: introductory
Prereqs:
  Labs:
    - '2205'  # OpenShift Cluster Installation
  Checklist:
    - Label: Verify cluster access
      Description: |
        Ensure you can access your cluster:
        ```bash
        oc get nodes
        ```
    - Label: Check default project
      Description: |
        Verify you can create resources:
        ```bash
        oc new-project demo
        ```

LabSection:
  Name: Deploy Sample Application
  Sections:
    - Name: Create Application
      Tabbed: setup-method
      Sections:
        - Name: Using the Portal
          Sections:
            - Objective: Open the OpenShift Web Console URL
              Description: |
                Use the URL from lab 1305, or retrieve it with:
                ```bash
                drpcli profiles get os-lab param openshift/console
                ```

            - Objective: Create a new project named "demo"
              Description: |
                1. Click "Projects" in the navigation menu
                2. Click "Create Project"
                3. Enter "demo" as the name
                4. Click "Create"

            - Objective: Deploy the sample application
              Description: |
                1. Click "+Add" in the project navigation
                2. Choose "Container Images"
                3. Enter "openshift/hello-openshift" in the Image Name field
                4. Keep default name "hello-openshift"
                5. Click "Create"
                
                This deploys a simple web application that responds with "Hello OpenShift!"

        - Name: Using CLI
          Sections:
            - Objective: Create deployment using oc command
              Description: |
                ```bash
                # Create a new project
                oc new-project demo

                # Deploy the hello-openshift application
                oc new-app openshift/hello-openshift

                # Watch the deployment
                oc get pods -w
                ```

    - Name: Expose Application
      Tabbed: setup-method
      Sections:
        - Name: Using the Portal
          Sections:
            - Objective: Create a route to the application
              Description: |
                1. Click "Networking" → "Routes" in the navigation
                2. Click "Create Route"
                3. Enter details:
                   - Name: hello-openshift
                   - Service: hello-openshift
                   - Target Port: 8080 → 8080
                4. Click "Create"

        - Name: Using CLI
          Sections:
            - Objective: Create route using oc command
              Description: |
                ```bash
                # Create route to expose the service
                oc expose service/hello-openshift

                # Get the route URL
                oc get route hello-openshift
                ```

    - Name: Verify Application
      Sections:
        - Objective: Test the application
          Description: |
            ```bash
            # Get the route URL
            ROUTE_URL=$(oc get route hello-openshift -o jsonpath='{.spec.host}')
            
            # Test the application
            curl http://$ROUTE_URL
            ```
            
            You should see the response: "Hello OpenShift!"

        - Objective: Scale the application
          Description: |
            ```bash
            # Scale to 3 replicas
            oc scale deployment/hello-openshift --replicas=3
            
            # Verify pods
            oc get pods
            
            # Check pod distribution
            oc get pods -o wide
            ```

Concepts:
  - openshift-deployment
  - kubernetes-services
  - routes
  - scaling
Tags:
  - openshift
  - application
  - deployment
  - networking
