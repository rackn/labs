 
# yaml-language-server: $schema=../../schema.json
Name: Manage Content in Git Repositories
Id: '2100'
Type: labs
Enabled: true
Available: true
Objective:
  Understand to store content in Git repositories and how to deploy content into the Digital Rebar Platform
Description: |+
  Digital Rebar Platform (DRP) manages content internally, and has capability to import content from a YAML or JSON 
  file that can be generated using source in any Source Code Management (SCM) system.
  This lab will guide you through the process of importing content from a Gitlab repository into the DRP.
  You will learn how to create a new repository, add content to it, and then import that content into the DRP.
  This is a fundamental skill for managing content in the DRP.

  Content is composed of several building blocks, including:
  - Params
  - Profiles
  - Templates
  - Tasks
  - Stages
  - Bootenvs
  - Workflows

  The content is stored in a directory structure that is then bundled into a single YAML or JSON file for import into the DRP.

  **Business ROI**: Consistent and repeatable content management is a key part of Infrastructure as Code (IaC)
Time: 10
Difficulty: intermediate
Prereqs:
  Labs:
    - '2000'
Concepts:
  - content
  - scm
  - git
Tags:
  - core
LabSection:
  Name: Summary
  Sections:
    - Name: git clone some example content
      Sections:
        - Objective: Clone the example "Color Demo" content from the RackN GitLab repository
          Description: |+
            ```shell
            git clone https://gitlab.com/rackn/example-content.git
            cd example-content/colordemo
            ``` 
        - Objective: Review the content
          Description: |+
            ```shell
            ls -l
            cat README.md
            cat conent/meta.yaml
            cat content/profiles/colordemo.yaml
            ```

            Note the Meta.color in the `colordemo.yaml` file.  It should be 'blue'.

    - Name: Bundle and Upload the "Color Demo" content
      Sections:
        - Objective: use `drpcli` to bundle the content
          Description: |+
            Make sure you are in the directory of the `example-content` repository containing the `meta.yaml` file when
            you run the `drpcli` commands.

            If you need a refresher on using `drpcli`, see [lab 2000](ux://labs/rackn/2000)


            ```shell
            cd colordemo/content
            drpcli contents bundle ../colordemo.yaml
            ```
        - Objective: use `drpcli` to upload the content
          Description: |+
            ```shell
            drpcli contents upload ../colordemo.yaml
            ```
        - Objective: validate the content is in the DRP using the UI
          Description: |+
            Open a browser to the DRP and browse to [Content](ux://Content) to and look for the `colordemo` content.
            It should be the same version you just uploaded.

            Browse to [Profiles](ux://Profiles) and look for the `colordemo-example` profile.
            It should have a blue icon, indicating it is the version you just uploaded.

    - Name: Make a change and re-upload the "Color Demo" content
      Sections:
        - Objective: make a change to the content
          Description: |+
            ```shell
            vi profiles/colordemo.yaml
            ```

            Change Meta.color to 'red' and save the file.

            ```shell
            vi meta.yaml
            ```

            Increment the last number of the version by one.  For example, if the version is 1.0.1, change it to 1.0.2.

            ```shell

        - Objective: use `drpcli` to bundle the content
          Description: |+

            ```shell
            drpcli contents bundle ../colordemo.yaml
            ```

            You'll now see a file at ../colordemo.yml that contains all the content in the color demo directory.

        - Objective: use `drpcli` to upload the content
          Description: |+
            ```shell
            drpcli contents upload ../colordemo.yaml
            ```
        - Objective: validate the content is in the DRP using the UI
          Description: |+
            Open a browser to the DRP and browse to [Profiles](ux://Profiles) to and look for the `colordemo` profile.
            It should now have a red icon, indicating it is the new version you just uploaded.

    - Name: Summary
      Description: |+
        You have now 

        1. cloned content from a GitLab repository
        2. made a change to it
        3. uploaded it into the DRP
        4. validated the content in the DRP

        This is the development cycle for content in the DRP that you will build on as you learn more about the platform.        
        You can now move on to the next lab.
