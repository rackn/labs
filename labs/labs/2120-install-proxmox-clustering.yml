# yaml-language-server: $schema=../../schema.json
Name: Install Proxmox Clustering
Id: '2120'
Type: labs
Enabled: true
Available: true
Objective: Provision Proxmox on bare metal, and create a cluster against it

Description: |+
  Proxmox is a comprehensive virtualization platform, enabling the creation
  and management of virtual machines and containers. This lab guides you
  through the Proxmox installation process on a bare metal server, followed
  by setting up a resource broker to provision VMs onto the Proxmox server.
  The final step involves creating a cluster of VMs within the Proxmox
  environment, utilizing PXE booting to simulate additional bare metal
  servers. This is particularly beneficial for testing and development
  scenarios.

  Requirements:
  - A machine equipped with a network bootable NIC, a primary OS disk, and a secondary disk for VM storage.
  - Digital Rebar Platform (DRP) installed and operational, with an active [Subnet](ux://Subnets).
  - The machine should be connected to the designated subnet.
  - A valid certificate for the Proxmox instance (Note: Creation of self-signed certificates is outside the scope of this lab).

  Business ROI: Implementing virtualization via Proxmox enhances resource efficiency and utilization
Time: 20
Difficulty: intermediate
Prereqs:
  Labs:
    - '2100'
Concepts:
  - bootenvs
  - clusters
  - resource brokers
  - virtual machines
  - bare metal
Tags:
  - core
  - provisioning
LabSection:
  Name: Install Proxmox Clustering
  Sections:
    - Name: Install Proxmox on a machine
      Description: Begin by preparing a new machine for Proxmox installation and note its disk configuration for subsequent steps.
      Sections:
        - Name: Boot a new machine and let DRP discover it
          Description: |+
            You will need to note the machine's primary and secondary disk devices,
            as they will be used for storage in the Proxmox installation.  When the machine
            boots in discovery mode, DRP takes inventory of the machine's hardware.

        - Name: Note the machine's secondary disk
          Description: |+
            The Proxmox installation will use the secondary disk for storage. 
            Look at the [machine's](ux://machines) params to find the name of the device for the primary and secondary disks.
            You will need to note the device names, as they will be used in the Proxmox installation.

            These are usually `/dev/sda` and `/dev/sdb` or `/dev/vda` and `/dev/vdb`, but they could be different.

        - Name: Apply configuration to the machine
          Sections:

            - Name: Create a new profile for Proxmox configuration
              Sections:

              - Objective: Create SSL Certs
                Description: |+
                  This profile will be used to fill in the details of the
                  Proxmox installation.
                  You will need to create a self-signed certificate and key to
                  use for the Proxmox installation, which is outside the scope
                  of this lab.

                  See also:
                  * [Create a self-signed cert](https://www.baeldung.com/openssl-self-signed-cert)

                  ```yaml
                  proxmox/certificate-crt: < a self-signed certificate >
                  proxmox/certificate-key: < a self-signed certificate key >
                  ```

                  For the certificate keys, take the full text out of the certificate
                  files and paste it into the appropriate paramerter values.
                  
              - Objective: Create disk configuration
                Description: |+
                  Note that you need to replace the text `/dev/sdb" with the large
                  VM storage disk you noted in a previous step.

                  ```yaml
                    proxmox/flexiflow-create-storage:
                      - proxmox-storage-setup-lvmthin
                    proxmox/storage-config:
                      lvmthin:
                        local-lvm:
                          content: images,rootdir,snippets,iso,vztmpl,backup
                          device: /dev/sdb
                          format: raw,qcow2,vmdk,subdir
                          name: local-lvm
                  ```

              - Objective: Set the network related parameters
                Description: |+ 
                  The `proxmox/api-url` and `proxmox/node` parameters are used to connect to the Proxmox API.

                  ```yaml
                  dns-domain: example.net
                  proxmox/api-url: 'https://{{.Machine.ShortName}}.{{.Param "dns-domain"}}:8006/api2/json'
                  proxmox/node: {{.Machine.ShortName}}
                  proxmox/vm-external-bridge: vmbr0
                  proxmox/bridge: vmbr0
                  proxmox/pve-enterprise-repo-enable: false
                  proxmox/flexiflow-debian-install:
                    - network-convert-interface-to-bridge

                  ntp-servers:
                    - 0.us.pool.ntp.org
                    - 1.us.pool.ntp.org
                    - 2.us.pool.ntp.org
                  ```

              - objective: Set the remaining parameters
                Description: |+
                  We won't describe these in detail, but they are needed.
                  ```yaml
                  provisioner-default-user: root
                  cluster/wait-filter: Or(Runnable=Eq(false),WorkflowComplete=Eq(true))
                  proxmox/gpg-keys:
                    - http://download.proxmox.com/debian/proxmox-release-bookworm.gpg
                    - http://download.proxmox.com/debian/proxmox-release-bullseye.gpg
                  proxmox/strip-kernel: false
                  proxmox/data-profile: testing-profile
                  # extra-packages: []
                  reboot-after-install: true
                  access-keys: {
                    "yourkey": "ssh-key ..... user@host"
                  }
                  ```
              
            - Objective: Attach profile `universal-application-ridge-road-proxmox-8`
              Description: |+
                This will also change the pipline setting over to `proxmox-8`.
              
        - Objective: Attach the workflow `universal-linux-install` and run to complete
          Description: |+
            This will start the installation of the underlying OS for the Proxmox installation, 
            in this case, Debian 12. When the workflow is complete, you'll be booted into Debian 12.

        - Objective: Attach the workflow `universal-proxmox` to the machine and run to complete
          Description: |+
            This will start the installation of Proxmox on the machine. When the workflow is complete,
            you'll have a configured Proxmox instance ready to deploy VMs.

            Open a web browser to https://<machine's ip>.<dns-domain>:8006 and log in with
            user `admin` the password set to `RocketSkates`, and the Realm set to "Proxmox VE Authentication Server"
        
    - Name: Create a resource broker
      Sections:
        - Objective: Add a new [resource broker](ux://resource_brokers)
          Sections:
            - Objective: Click on Add
            - Objective: Select "resource-proxmox-broker"
            - Objective: Fill in the required fields
              Description: |+
                You will need to fill in the required fields, including the Proxmox API URL, 
                the Proxmox node, and the Proxmox user and password.

                * `proxmox/node`: The name of the Proxmox node, should be the same as the machine's short name
                * `proxmox/user`: "admin@pve" # The user to log into the Proxmox API
                * `proxmox/password`: "RocketSkates" # The password to log into the Proxmox API
                * `proxmox/api-url`: "https://<machine's shortname>.<dns-domain>:8006/api2/json" # The URL to the Proxmox API
                * `proxmox/tls-insecure`: on # This is needed for self-signed certs
            - Objective: Click on Save
            - Objective: Watch as the resource broker is created and transitions to completed and locked
    - Name: Add a new [cluster](ux://clusters)
      Sections:
        - Objective: Click on Add
        - Objective: For the field "broker/name" select the resource broker created in the previous step
        - Objective: Save the cluster
        - Objective: Watch as the cluster is created, and 3 new vms are created


    - Name: Verify VMs are booting into sledgehammer
      Description: |+
        The VMs should be booting into the sledgehammer environment, which is the first step in the boot process for a machine that is being provisioned by Digital Rebar.
        This step will check on progress of the VMs.
      Sections:
        - Objective: Log into Proxmox
          Description: |+
            Open a web browser to https://<machine's ip>.<dns-domain>:8006 and log in with
            user `admin` the password set to `RocketSkates`, and the Realm set to "Proxmox VE Authentication Server"
        - Objective: Click on "Datacenter"
        - Objective: Click on The shortname of your machine
        - Objective: Click on the "Console" tab for VMs that show up under that cluster
        - Objective: Check the consoles for activity
          Description: |+
            You should see the machines attempt to network boot, get a PXE boot response and start to load the sledgehammer environment.
            which is the first step in the boot process for a machine that is being managed by Digital Rebar.
    