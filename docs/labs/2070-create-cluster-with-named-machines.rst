.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Create a Cluster with Named Machines; Labs

.. _rs_cp_2070:

2070 Create a Cluster with Named Machines
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 2070
* Time: 15 Minutes
* Enabled: Yes
* Difficulty: intermediate
* Tags: clusters, profiles, resource_brokers
* Concepts: params, clusters

Objective
---------

Learn how to define a cluster to enable specific control of which
machines are present or to have machines in multiple roles


  Use the more advanced machine naming capabilities of clusters, the User
  will create a cluster with customized machine names and roles.
  
  **Business ROI**: Maintaining production clusters requires operators to
  maintain exact control of the lifecycle of all managed systems.


Prerequisites
-------------
Required Labs:

* 2060



Summary
=======

Create an empty cluster
+++++++++++++++++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Click ``Add``
   
      Set ``Name`` to ``lab2070``
      
      Set the ``broker/name`` to ``context-broker``
      
      Set the ``cluster/count`` to 0
      
      With this is 0, the cluster logic will not allocate any machines. This
      gives us a chance to build a Parameter that defines the cluster.
      
      Click ``Save`` and wait for it to enter Work Order Mode
   


Create a named set of machines for the cluster
++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to the `lab2070 <https://portal.rackn.io/#/e/0.0.0.0/clusters/lab2070/params>`__ cluster’s
      Params tab
   
   
   
   
   #. Click ``Add Params`` and select ``cluster/machines``
   
   
   
   
   #. Click ``Add Params`` and select ``cluster/machine-types``
   
   
   
   
   #. Set the ``cluster/machines-types`` paramter value
   
      The default value is acceptable.
      
      .. code:: json
      
         [ "machine" ]
   
   
   
   
   #. Set the ``cluster/machines`` paramter value
   
      Set the value to:
      
      .. code:: json
      
         {  
           "machine": { 
               "names": [ "lab2070-red", "lab2070-green", "lab2070-blue" ] 
           }
         }
      
      Click the little ``save`` icon
      
      Hover over the ``cluster/machines`` text to review the inline help for
      formatting
   
   
   
   
   #. Navigate to the `Activity <https://portal.rackn.io/#/e/0.0.0.0/clusters/lab2070/activity>`__ tab
   
   
   
   
   #. Click ``Apply`` to run the ``universal-application-base-cluster`` Work
      Order
   
      This Work Order reevaluates the parameters of the cluster to grow or
      shrink the cluster.
   


Observe the cluster being built
+++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
   
   
   
   #. Observe the named machines being created
   
      As the cluster builds, machines named ``lab2070-red``,
      ``lab2070-green``, and ``lab2070-blue`` will appear.
   


Create a named set of machines for the cluster
++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to the `lab2070 <https://portal.rackn.io/#/e/0.0.0.0/clusters/lab2070/params>`__ cluster’s
      Params tab
   
   
   
   
   #. Edit the ``cluster/machines`` paramter value
   
      Set the value to:
      
      .. code:: json
      
         {  
           "machine": { 
               "names": [ "lab2070-red", "lab2070-yellow", "lab2070-blue" ] 
           }
         }
      
      Click the little ``save`` icon
   
   
   
   
   #. Navigate to the `Activity <https://portal.rackn.io/#/e/0.0.0.0/clusters/lab2070/activity>`__ tab
   
   
   
   
   #. Click ``Apply`` to run the ``universal-application-base-cluster`` Work
      Order
   
      This Work Order reevaluates the parameters of the cluster to grow or
      shrink the cluster.
   


Observer the cluster being updated
++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
   
   
   
   #. Observe the named machines being created and removed
   
      As the cluster updates, the machine ``lab2070-green`` will be removed
      and the ``lab2070-yellow`` machine will be added.
   


Create a new class of named set of machines for the cluster
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to the `lab2070 <https://portal.rackn.io/#/e/0.0.0.0/clusters/lab2070/params>`__ cluster’s
      Params tab
   
   
   
   
   #. Edit the ``cluster/machine-types`` paramter value
   
      Make the value look like this:
      
      .. code:: json
      
         [ "machine", "leader" ]
   
   
   
   
   #. Edit the ``cluster/machines`` paramter value
   
      Set the value to:
      
      .. code:: json
      
         {  
           "machine": { 
               "names": [ "lab2070-red", "lab2070-yellow", "lab2070-blue" ] 
           },
           "leader": {
               "names": [ "lab2070-leader" ]
           }
         }
      
      Click the little ``save`` icon
   
   
   
   
   #. Navigate to the `Activity <https://portal.rackn.io/#/e/0.0.0.0/clusters/lab2070/activity>`__ tab
   
   
   
   
   #. Click ``Apply`` to run the ``universal-application-base-cluster`` Work
      Order
   
      This Work Order reevaluates the parameters of the cluster to grow or
      shrink the cluster.
   


Observer the cluster being updated to have a leader
+++++++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
   
   
   
   #. Observe the new leader named machine being created
   
      As the cluster updates, the machine ``lab2070-leader`` will be added.
   


Make a templatized set of named machines for a cluster
++++++++++++++++++++++++++++++++++++++++++++++++++++++

   The named machines pattern is useful, but has “hard-coded” values for
   the names within the cluster. There are times when a template of machine
   names is useful. Part of the name should reflect the cluster name, but
   the rest of the name should follow the defined patterns.
   
   This section will walk through creating a template profile.
   
   
   
   #. Navigate to `Profiles <https://portal.rackn.io/#/e/0.0.0.0/profiles>`__
   
   
   
   
   #. Click ``Create``
   
      Set ‘Name’ to ``template2070``
      
      Click ‘Save’
   
   
   
   
   #. Click ``Add Params`` and add ``cluster/machine-types`` and
      ``cluster-machines``
   
   
   
   
   #. Edit ``cluster/machine-types``
   
      Set the value to:
      
      .. code:: json
      
         [ "machine",  "leader" ]
      
      Click the little ``save`` icon
   
   
   
   
   #. Edit ``cluster/machines``
   
      Set the value to:
      
      .. code:: json
      
         {
           "leader": {
             "names": [
               "{{.Machine.Name}}-leader"
             ]
           },
           "machine": {
             "names": [
               "{{.Machine.Name}}-red",
               "{{.Machine.Name}}-green",
               "{{.Machine.Name}}-yellow"
             ]
           }
         }
      
      Click the little ``save`` icon
      
      The ``{{.Machine.Name}}`` is Golang template expansion markers that DRP
      uses to templatize a value. In this case, the cluster name is
      represented by ``{{.Machine.Name}}``.
   


Create a couple of clusters using the template
++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Click ``Add``
   
      Set ``Name`` to ``lab2070-t1``
      
      Add the template profile, ``template2070``, by clicking the ``+`` next
      to Configuration / Profiles
      
      Set the ``broker/name`` to ``context-broker``
      
      Click ``Save``
      
      ``cluster/count`` is ignored because the override by the template
      profile.
   
   
   
   
   #. Close the ``lab2070-t1`` editor by clicking the ``x``
   
   
   
   
   #. Click ``Add``
   
      Set ``Name`` to ``lab2070-t2``
      
      Add the template profile, ``template2070``, by clicking the ``+`` next
      to Configuration / Profiles
      
      Set the ``broker/name`` to ``context-broker``
      
      Click ``Save``
      
      ``cluster/count`` is ignored because the override by the template
      profile.
   
   
   
   
   #. Close the ``lab2070-t2`` editor by clicking the ``x``
   


Observer the clusters being built
+++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
   
   
   
   #. Observe the named machines being created both clusters
   
      As the clusters build, two sets of machines will be created with
      ``lab2070-t1-`` and ``lab2070-t2-``.
      
      The clusters are built in parallel once created.
   


Clean up the clusters
+++++++++++++++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Select all three clusters (``lab2070``, ``lab2070-t1``, ``lab2070-t2``)
      in the table
   
   
   
   
   #. Click ``Actions`` and then select ``Work Flow Mode``
   
   
   
   
   #. Click ``Actions`` and then select ``Delete (Cleanup)``
   
   
   
   
   #. Click ``Cleanup`` to confirm the action
   
   
   
   
   #. Wait for the clusters and machines to be removed
   



