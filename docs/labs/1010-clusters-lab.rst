.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Manage Groups of Machines using Clusters; Labs

.. _rs_cp_1010:

1010 Manage Groups of Machines using Clusters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1010
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: clusters, core
* Concepts: clusters, resource_brokers, machines, contexts
..  youtube:: osv6vHFEhk0
    :privacy_mode:
`Video Link <https://www.youtube.com/watch?v=osv6vHFEhk0>`__

Objective
---------

Learn how Digital Rebar manages infrastructure using IaC resource
abstractions like Clusters and Brokers.


  This is our most basic IaC learning module that shows the most basic
  infrastructure management operations in Digital Rebar.
  
  **Business ROI**: Unlike multi-tenant SaaS, Digital Rebar is user
  operated software: you will never have to open your firewall for us,
  share sensitive information or upload access credentials.


Prerequisites
-------------
Required Labs:

* 1005



Summary
=======

   Use IaC to create a group of machines that are coordinated using an
   dedicated lifecycle workflow object. In Digital Rebar, that known as a
   “cluster”

Creating a Cluster
++++++++++++++++++
   
   
   
   #. Navigate to the `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ view
   
   
   
   
   #. Click the ``Add`` Button.
   
      Name your Cluster ``lab1010``
      
      Set ``broker/name`` Parameter to ``context-broker``
      
      Change ``cluster/count`` if you want more or less than 3 machines
      
      Click ``Save``
      
      We will reconfigure the ``cluster/count`` in a later step.
      
      Now, watch the `lab1010 Activity
      Tab <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1010/activity>`__ for the
      ``cluster-provision`` task
   


Observing the Operation
+++++++++++++++++++++++

   Now that you’ve created a cluster, let’s what Digital Rebar is doing as
   an IaC platform on your behalf.
   
   
   
   #. Scan Digital Rebar
   
      From Resource Brokers, review the
      `context-broker <https://portal.rackn.io/#/e/0.0.0.0/resource_brokers/Name:context-broker/activity>`__
      to observe the Activity
      
      From `Overview <https://portal.rackn.io/#/e/0.0.0.0/overview>`__, observe the Cluster Status Heartbeat
      and Runtime Statistics
      
      *Note: The Heartbeat only shows active clusters. If your cluster has
      completed operations, then it will not appear.*
      
      From `Work Orders <https://portal.rackn.io/#/e/0.0.0.0/work_orders>`__, observe the Work Order activity
      
      From `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__, observe the newly created
      ``lab1010-#`` Machines that are part of the ``lab1010`` cluster
   
   
   
   
   #. Observe that the your `lab1010
      Machines <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab1010-0/profiles/lab1010>`__ and cluster
      have shared profile (lab1010).
   
      The `shared profile <https://portal.rackn.io/#/e/0.0.0.0/profiles/lab1010>`__ is created when you
      created the lab and stores information that becomes accessible to the
      machines in the cluster because the cluster’s profile is also assigned
      to the machines. A share profile is common Digital Rebar practice used
      to create a shared working configuration between different IaC
      components.
   
   
   
   
   #. From one of your `lab1010 Machines <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab1010-0>`__,
      use the “Cluster” field to navigate back to the **lab1010** cluster.
   
      Digital Rebar stores the relationship between a machine and the
      cluster(s) that are managing it in the ``cluster/tags``
      `Param <https://portal.rackn.io/#/e/0.0.0.0/machines/Name:lab1010-0/params>`__.
      
      While looking at the ``cluster/tags`` Param, you should also note that
      Digital Rebar also tracks information about the broker that created it
      in Params like ``broker/name``.
      
      Params are used extensively throughout the platform to capture data
      about systems and their relationships.
   


Resizing your `lab1010 Cluster <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1010>`__
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

   In Day 2 IaC, we must assume that everything we create will be modified.
   In this step, we’ll show how to resize the cluster you just created.
   Digital Rebar deliberately separates configuration and action
   operations. That ensures that you have time to complete all
   configuration changes before Digital Rebar begins making changes. Future
   labs will show you how to use Triggers to act immediately on changes.
   
   
   
   #. Drain the `lab1010 Cluster <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1010/>`__
   
      Change the ``cluster/count`` value to ``0``
      
      Select and Apply the ``universal-application-base-cluster`` Blueprint.
      
      The `lab1010 Machines <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1010/machines>`__ tab
      should not have any more machines listed.
      
      It is acceptable to have an empty cluster (count = zero) with no
      machines.
      
      This is different from deleting the cluster because all of the cluster
      APIs and configuration are available in an empty cluster. In an IaC
      sense, you can completely drain all the resources from a cluster and
      then reconstruct the cluster later by simply increasing the count.
   
   
   
   
   #. Resize and Update `lab1010 Cluster <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1010/>`__
   
      Change the ``cluster/count`` value to ``5``
      
      Select and Apply the ``universal-application-base-cluster`` Blueprint.
      
      This will cause the cluster to re-evaluate it’s configuration.
      
      You can watch the change from the `lab1010
      Machines <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1010/machines>`__ tab
      
      If you try different ``cluster/count``, you will notice that Digital
      Rebar will add or remove machines during the re-evaluation phase.
      
      Note: Future labs will explain how to control the naming of machines in
      your cluster.
   


Cleanup your cluster
++++++++++++++++++++

   Releasing resources is a critical part of the IaC story. When you
   cleanup your cluster, Digital Rebar invokes the Workflow defined in
   ``on-delete-workflow`` to automatically decommission the cluster. This
   includes telling the associated resource broker to remove all the
   associated machines. It’s important to understand, this is intentional
   IaC behavior that was added on your behalf when you created the cluster.
   If you delete a cluster without this setting, then it will leave the
   associated machines in place. This is also helpful if you’ve made a
   mistake with your lab and need to reset and start over.
   
   
   
   #. From the `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ table
   
      Select row with lab1010 (a checkbox will appear)
      
      Use the Actions list to return your Cluster to **Workflow Mode**
      
      Use the Actions list to **Cleanup** your cluster (the related machines
      will be automatically removed also)
   



