.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Tour the Digital Rebar UX; Labs

.. _rs_cp_1005:

1005 Tour the Digital Rebar UX
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1005
* Time: 5 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: how-to, ux
* Concepts: ux

Objective
---------

Learn How to Navigate the UX


  Review and describe all UX area and components for managing Digital
  Rebar
  
  **Business ROI** The RackN Portal for Digital Rebar is a comprehensive
  front end for Infrastructure as Code features of the platform.


Prerequisites
-------------
Required Labs:

* 1001



How to Navigate the UX
======================

General Layout
++++++++++++++
   
   
   
   #. Starting `here <https://portal.rackn.io/#/e/0.0.0.0/overview>`__
   
   
   
   
   #. The Nav Tree on the left handles selecting components of the system to
      operate on.
   
      There are collapsible sections for related components. These will be
      explore individually in the next sections of this lab.
      
      The bottom of the Nav Tree has general version info and helpful cut and
      paste buttons for support information.
      
      Nav Bar Elements (from top to bottom)
      
      -  Id of currently viewed DRP Endpoint
      -  Events viewer icon (megaphone icon) that opens dialog to see events
      -  UX Views Role (if ux-views plugin is loaded) that selects views that
         alter the UX layout.
      -  Database issues, visible if there are issues with the database.
      -  Alerts notification bar, visible if alerts are pending.
      -  Navigation links to elements.
      -  General version info and support helpers at the bottom of the list
   
   
   
   
   #. The Control Bar at the very top of the page (black background) has
      site-wide operations
   
      Common elements (from left to right)
      
      -  UX selector if not on stable (fire icon)
      -  RackN icon links to RackN `home page <https://rackn.com/>`__
      -  Endpoint selector - a dropdown of endpoints the UX has visited.
      -  Search bubble to search the ux and docs for items
      -  Labs button to connect to the lab subsystem.
      -  User button of the currently logged in user. Directs to the User’s
         object in the UX.
      -  Logout button
   


Left Navigation Sections
++++++++++++++++++++++++
   
   
   
   #. System Section
   
      Controls and Support for the Digital Rebar Endpoint
      
      The `Overview <https://portal.rackn.io/#/e/0.0.0.0/overview>`__ page provides live updated system level
      information. This page has four dynamically updated Sections:
      
      -  Cluster Cost determined using the `inventory cost
         calculation <https://portal.rackn.io/#/e/0.0.0.0/tasks/inventory-cost-calculator>`__ task.
      -  Pool Activity (requires `Pools <https://portal.rackn.io/#/e/0.0.0.0/pools>`__ to be configured)
         shows machine allocation in defined pools.
      -  Machine Stages shows active states for all managed machines
      -  Runtime Statistics (if enabled) shows a whisker distribution for work
         performed by Digital Rebar
      
      The `Alerts <https://portal.rackn.io/#/e/0.0.0.0/alerts>`__ page provides Operational alerts for the
      system
      
      The `Plugins <https://portal.rackn.io/#/e/0.0.0.0/plugins>`__ page provides Installed binary extensions
      to enhance Digital Rebar
      
      The `Info & Preferences <https://portal.rackn.io/#/e/0.0.0.0/system>`__ page provides System Wizard,
      Health Check and Configuration. This page provides key insights about
      the system.
      
      The `License Manager <https://portal.rackn.io/#/e/0.0.0.0/license>`__ page provides Check and update
      RackN Entitlements. Use this page to update expired or changed licenses.
      
      The `Inbox <https://portal.rackn.io/#/e/0.0.0.0/inbox>`__ page provides Communicate with RackN. Note
      that this is a “drop box” and not a real-time chat. Please use
      `Slack <https://rackn.com/slack>`__ if you want chat features.
   
   
   
   
   #. Resources Section
   
      Infrastructure Managed by Digital Rebar
      
      The `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ page provides Virtual and physical
      infrastructure including Servers, Storage and Switches. This is one of
      the most used pages in the UX and provides both comprehensive inventory
      and easy operational control.
      
      The `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ page provides Life-cycle management for
      groups of machines.
      
      The `Resource Brokers <https://portal.rackn.io/#/e/0.0.0.0/resource_brokers>`__ page provides Interface
      for requesting with machine resources through pools and external system.
      
      The `Pools <https://portal.rackn.io/#/e/0.0.0.0/pools>`__ page provides Elastic groups of machines with
      automatic in/out provisioning operations.
      
      The `Racks <https://portal.rackn.io/#/e/0.0.0.0/racks>`__ page provides Track physical machine
      placement and equipment manifests
      
      \*Note: requires installation of the `Racks <https://portal.rackn.io/#/e/0.0.0.0/catalog/racks>`__
      plugin!*\*
   
   
   
   
   #. Services Section
   
      Operations performed on machines by Digital Rebar
      
      The `Jobs <https://portal.rackn.io/#/e/0.0.0.0/jobs>`__ page provides Logs generated during Workflow
      and Work Order operations
      
      The `Work Orders <https://portal.rackn.io/#/e/0.0.0.0/work_orders>`__ page provides Requests for
      operations to be performed on resources running in Work Order mode
      
      The `Triggers <https://portal.rackn.io/#/e/0.0.0.0/triggers>`__ page provides Initiate Operations based
      behaviors or external events
      
      The `Blueprints <https://portal.rackn.io/#/e/0.0.0.0/blueprints>`__ page provides Reusable templates
      used to create Work Orders based on IaC patterns
      
      The `Trigger Providers <https://portal.rackn.io/#/e/0.0.0.0/trigger_providers>`__ page provides
      Interfaces for timers and external triggers
   
   
   
   
   #. Multi-Site Section
   
      Tools for managing distributed control plane of Digital Rebar servers
      
      The `Endpoints <https://portal.rackn.io/#/e/0.0.0.0/endpoints>`__ page provides Remote subscribed
      Digital Rebar servers
      
      The `Version Sets <https://portal.rackn.io/#/e/0.0.0.0/version_sets>`__ page provides Collections of
      IaC content to be assigned to managed Endpoints
   
   
   
   
   #. Networking Section
   
      Procides DHCP related utilities.
      
      *Note: DHCP services are mainly used for Bare Metal provisioning use
      cases*
      
      The `Subnets <https://portal.rackn.io/#/e/0.0.0.0/subnets>`__ page provides Network Interfaces enabled
      for DHCP
      
      The `Leases <https://portal.rackn.io/#/e/0.0.0.0/leases>`__ page provides IP addresses assigned by the
      DHCP system
      
      The `Reservations <https://portal.rackn.io/#/e/0.0.0.0/reservations>`__ page provides IP addresses
      pre-allocated for the DHCP system
   
   
   
   
   #. Provision Section
   
      IaC components for provisioning automation
      
      The `Boot Environments <https://portal.rackn.io/#/e/0.0.0.0/bootenvs>`__ page provides Information
      needed during Network Booting different operating systems.
      
      Most BootEnvs support Bare Metal Provisioning and need supporting ISOs;
      however, several are general purpose
      
      -  `discovery <https://portal.rackn.io/#/e/0.0.0.0/bootenvs/discovery>`__ is the Digital Rebar
         discovery environment
      -  `sledgehammer <https://portal.rackn.io/#/e/0.0.0.0/bootenvs/sledgehammer>`__ is the O/S for
         discovery when needed
      -  `local <https://portal.rackn.io/#/e/0.0.0.0/bootenvs/local>`__ uses the local O/S
      -  `ignore <https://portal.rackn.io/#/e/0.0.0.0/bootenvs/ignore>`__ tells Digital Rebar to not take
         action
      
      The `Contexts <https://portal.rackn.io/#/e/0.0.0.0/contexts>`__ page provides Containerized Agents
      managed by Digital Rebar. Of these, the
      `drpcli-runner <https://portal.rackn.io/#/e/0.0.0.0/contexts/drpcli-runner>`__ is needed to provide a
      reliable default. Others are loaded as needed for specialized task
      handling.
      
      The `Params <https://portal.rackn.io/#/e/0.0.0.0/params>`__ page provides Variables defined for Digital
      Rebar automation. Digital Rebar does NOT require operators pre-define
      Params before using them; however, best IaC practice is to define and
      document all used parameters.
      
      The `Profiles <https://portal.rackn.io/#/e/0.0.0.0/profiles>`__ page provides Allows grouping and
      nesting of Params. The `global Profile <https://portal.rackn.io/#/e/0.0.0.0/profiles/global>`__ is a
      special profile that is always included in every param expansion.
      
      The `Raid <https://portal.rackn.io/#/e/0.0.0.0/raid>`__ page provides Definition for RAID systems
   
   
   
   
   #. Control Section
   
      IaC Automation control flow components
      
      The `Pipelines <https://portal.rackn.io/#/e/0.0.0.0/pipelines>`__ page provides Combination of
      configuration and workflow chains for end-to-end processes
      
      The `Workflows <https://portal.rackn.io/#/e/0.0.0.0/workflows>`__ page provides Multi-step automation
      process for machine configuration
      
      The `Stages <https://portal.rackn.io/#/e/0.0.0.0/stages>`__ page provides Subcomponent of a Workflow
      enabling reusable components and parameters
      
      The `Tasks <https://portal.rackn.io/#/e/0.0.0.0/tasks>`__ page provides Operational unit of work for
      Stages and Work Orders
      
      The `Templates <https://portal.rackn.io/#/e/0.0.0.0/templates>`__ page provides Reusable module used by
      other IaC components to keep code DRY
   
   
   
   
   #. IaC Artifacts Section
   
      Atomic components stored and managed by Digital Rebar
      
      The `Catalog <https://portal.rackn.io/#/e/0.0.0.0/catalog>`__ page provides Inventory, install,
      download, upgrade and remove Content Bundles. This view also can
      sequence and perform Digital Rebar upgrades including of the Endpoint.
      
      The `Contents <https://portal.rackn.io/#/e/0.0.0.0/content_browser>`__ page provides Graphical
      navigation of all installed content grouped by Content Bunndle. This
      view can help find and understand how IaC components are bundled
      together.
      
      The `Boot ISOs <https://portal.rackn.io/#/e/0.0.0.0/isos>`__ page provides Installation Media used for
      Network Booting with BootEnvs.
      
      *Note: ISOs are mainly used for Bare Metal provisioning use cases*
      
      The `Files <https://portal.rackn.io/#/e/0.0.0.0/files>`__ page provides DRPCLI and general purpose file
      storage needed for IaC automation
   
   
   
   
   #. Endpoint Admin Section
   
      Endpoint Operational Controls
      
      The `Logs <https://portal.rackn.io/#/e/0.0.0.0/logs>`__ page provides Digital Rebar operational history
      
      The `Users <https://portal.rackn.io/#/e/0.0.0.0/users>`__ page provides Authorized Operators of Digital
      Rebar
      
      The `Roles <https://portal.rackn.io/#/e/0.0.0.0/roles>`__ page provides Enables groups of users to be
      given specific permissions
      
      The `Tenants <https://portal.rackn.io/#/e/0.0.0.0/tenants>`__ page provides Groups of Users with
      resource restricted access
      
      *Note: the following items require installation of the*\ `UX
      Views <https://portal.rackn.io/#/e/0.0.0.0/catalog/ux-views>`__\ *plugin!*
      
      The `UX Views <https://portal.rackn.io/#/e/0.0.0.0/ux_views>`__ page provides Role configuration for
      specialized UX overrides and behaviors
      
      The `UX Config <https://portal.rackn.io/#/e/0.0.0.0/ux_config>`__ page provides User configuration for
      UX overrides and behaviors
      
      *Note: the following item requires installation of the LDAP plugin!*
      
      The `Identity Providers <https://portal.rackn.io/#/e/0.0.0.0/identity_providers>`__ page provides
      External authentication (SAML, Active Directory, etc)
   



