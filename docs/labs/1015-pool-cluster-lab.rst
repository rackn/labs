.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Build a Cluster using Machine Pools; Labs

.. _rs_cp_1015:

1015 Build a Cluster using Machine Pools
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1015
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: pool, metal, self-service
* Concepts: clusters, resorce brokers
..  youtube:: XaiIQKkcNWo
    :privacy_mode:
`Video Link <https://www.youtube.com/watch?v=XaiIQKkcNWo>`__

Objective
---------

Use the Pool Broker to allocate multiple clusters of machines from
shared set of existing machines. Very helpful for limited resources or
machines with long setup cycles.


  **Business ROI**: Treat bare metal machines as a dynamic shared
  resource.


Prerequisites
-------------
Required Labs:

* 1010



Summary
=======

Define a Pool
+++++++++++++
   
   
   
   #. Navigate to the `Pools <https://portal.rackn.io/#/e/0.0.0.0/pools>`__ table and click the ``Create``
      button
   
   
   
   
   #. Provide the Id as ``lab1015``
   
      Digital Rebar does NOT require pools to be pre-defined; however, you
      cannot define the insert/remove/allocate/release actions of a pool
      unless you have created a pool resource.
   
   
   
   
   #. Optionally, set ``Meta Icon`` to ``user``
   
      Setting the icon makes it easier to identify machines in our target
      pool.
      
      The Machines view does *not* use the color property for pools because
      pools are shown black if ``Free`` and green if ``InUse``.
   
   
   
   
   #. Save the Pool
   


Create the source ``lab1015`` Cluster to fill the pool
++++++++++++++++++++++++++++++++++++++++++++++++++++++

   This cluster will provide the machines for our test pool. In a bare
   metal environment, these would be provided through a discovery or
   registration process.
   
   
   
   #. Navigate to the `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ table and click the ``Add``
      button
   
      Name your Cluster ``lab1015``
      
      Choose the ``context-broker`` for the ``broker/name`` Parameter.
      
      Update the ``cluster/count`` to ``10``.
      
      Save the Cluster and allow it to progress to Work Order mode.
   
   
   
   
   #. Observe the Cluster’s Activity as it builds the Cluster to the
      “cluster-provision” task
   


Put the ``lab1015-#`` machines into the ``lab1015`` Pool
++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Select all ``lab1015-#`` from `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
   
   
   
   #. Insert all ``lab1015-#`` machines into the ``lab1015`` Pool
   
      Using the Actions…Pools…Insert menu option to show the actions panel
      Select Pool ``lab1015`` Click “Commit 1 action” to apply the change. You
      should see the Pool column change from boxes (default) to a user
      (lab1015) At this point, you should be able to see the instances created
      the target cloud.
   


Create a ``lab1015-broker`` Resource Broker using Pool ``lab1015``
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Resource Brokers <https://portal.rackn.io/#/e/0.0.0.0/resource_brokers>`__ click the
      ``Add`` button
   
   
   
   
   #. Select the ``resource-pool`` Resource Profile and name your broker
      ``lab1015-broker``
   
   
   
   
   #. Provide the Required Params
   
      Set ``broker-pool/pool`` to ``lab1015``
      
      Leave ``broker-pool/allocate`` set to true.
   
   
   
   
   #. Save the Broker
   
   
   
   
   #. Wait for the Broker to change into Work Order mode
   


Allocate the consumption Clusters
+++++++++++++++++++++++++++++++++

   Clusters that use the Pool Broker will allocate existing machines from
   the pools rather than creating new machines. When the machines are
   removed from the cluster, they are released back into the pool and the
   release action is applied.
   
   
   
   #. Navigate to the `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ table and click the ``Add``
      button
   
      Name your Cluster ``circle``
      
      Choose the ``lab1015-broker`` for the ``broker/name`` Parameter.
      
      Update the ``cluster/count`` to ``5``.
      
      Set the Icon to ``circle``
      
      Save the Cluster
   
   
   
   
   #. Observe the Cluster’s machines on the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ view.
   
      Notice that the Cluster Profile is added to the profiles list
      
      Notice that the PoolStatus is now InUse instead of Free
   
   
   
   
   #. Navigate to the `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ table and click the ``Add``
      button
   
      Name your Cluster ``square``
      
      Choose the ``lab1015-broker`` for the ``broker/name`` Parameter.
      
      Update the ``cluster/count`` to ``5``.
      
      Set the Icon to ``square``
      
      Save the Cluster
   
   
   
   
   #. Observe the Cluster’s machines on the `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ view.
   
      Notice that the Cluster Profile is added to the profiles list
      
      Notice that the PoolStatus is now InUse instead of Free
   


Explore the Pool allocation
+++++++++++++++++++++++++++

   The following are suggested activities to better understand how the pool
   broker allocates and releases resources from the pools.
   
   
   
   #. Increase the size of ``square`` to 6 and apply the
      ``universal-application-base-cluster`` blueprint
   
      Notice that request does not complete because the pool is out of
      machines
   
   
   
   
   #. Descreate the size of ``circle`` to 2 and apply the
      ``universal-application-base-cluster`` blueprint
   
      Notice that the released machines automatically run the
      ``load-generator`` workflow because it was assigned as the release
      action in the pool
   
   
   
   
   #. In ``square``, reapply the ``universal-application-base-cluster``
      blueprint
   
      Notice that request completes because machines have been freed from
      ``circle``
   


Cleanup your clusters
+++++++++++++++++++++
   
   
   
   #. From the `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ table, select ``square``, and
      ``circle`` to release the machines
   
      For safety reasons, you cannot delete machines that are InUse
   
   
   
   
   #. Use the Actions list to return your Cluster to ``Workflow mode``
   
   
   
   
   #. Use the Actions list to Cleanup your cluster (the related machines will
      be automatically removed also)
   
      Cleanup is a special version of Destroy that will run *and complete* the
      ``on-delete-workflow`` before invoking Destroy.
   
   
   
   
   #. Repeat the procdess for the ``lab1015`` cluster
   



