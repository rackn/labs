.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Build a Multi-Cloud Cluster using Pre-Defined Terraform; Labs

.. _rs_cp_1020:

1020 Build a Multi-Cloud Cluster using Pre-Defined Terraform
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1020
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: terraform, cloud, self-service
* Concepts: clusters, resorce brokers
..  youtube:: Q_VkODHftRo
    :privacy_mode:
`Video Link <https://www.youtube.com/watch?v=Q_VkODHftRo>`__

Objective
---------

Use Cluster Pipelines to build a dynamic set of machines via Terraform
plans without building plans


  **Business ROI**: Multi-cloud self-service process allows teams to share
  and reuse managed Terraform plans


Prerequisites
-------------
Required Labs:

* 1010

Addtional Checklist Items:

* Credentials for at least one Cloud (varies depending on the cloud)


Summary
=======

Add a Resource Broker
+++++++++++++++++++++
   
   
   
   #. Navigate to the `Resource Brokers <https://portal.rackn.io/#/e/0.0.0.0/resource_brokers>`__ table and
      click the ``Add`` button
   
   
   
   
   #. Select the appropriate ``*-cloud`` Resource Profile and name your broker
      ``lab1020-broker``
   
   
   
   
   #. Provide the information for the Required Params
   
      These vary depending on the cloud you are using
      
      **Design Note:** By setting credentials here, callers these the cloud
      credential profile will handle the security/authentication parameters
      for all clusters using the broker.
   
   
   
   
   #. Review the information for the Optional Params (clicking reveals the
      default)
   
      For aws, the “rsa/key-user” should be set to “ec2-user”
   
   
   
   
   #. Save the Broker
   
   
   
   
   #. Wait for the Broker to change into Work Order mode
   
      Troubleshooting note: the broker parameters are not exercised in this
      phase; consequently, configuration mistakes will not be revealed until
      the next step.
   
   
   
   
   #. Optional Setup
   
      If you’d like to review the generated Terraform Plans (they are not
      normally stored) then add the Param " terraform/debug-plan" to the
      broker and make sure it’s true. Plans are stored with the Terraform
      State on the invoking cluster.
   


Create the lab1020 Cluster
++++++++++++++++++++++++++
   
   
   
   #. Navigate to the `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ table and click the ``Add``
      button
   
      Name your Cluster ``lab1020``
      
      Choose the ``lab1020-broker`` for the “broker/name” Parameter.
      
      Update the cluster/count to match your planned cluster size.
      
      Save the Cluster and allow it to progress to Work Order mode.
   
   
   
   
   #. Observe the Cluster’s Activity as it builds the Cluster to the
      “cluster-provision” task
   


Review the Cluster
++++++++++++++++++
   
   
   
   #. From `Resource Brokers <https://portal.rackn.io/#/e/0.0.0.0/resource_brokers>`__
   
      Observe the Activity to see Terraform being run during the
      ``terraform-apply`` task.
   
   
   
   
   #. From `Work Orders activity <https://portal.rackn.io/#/e/0.0.0.0/work_orders>`__ wait for the
      ``terraform-apply`` to complete.
   
      At this point, you should be able to see the instances created the
      target cloud.
   
   
   
   
   #. From `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ watch for instances to register
   
      The ``terraform-apply`` task will add machines into Digital Rebar
      *before* they are created by the Cloud Provider so that they can join-up
      and have pre-defined operations as soon as they are provisioned.
   
   
   
   
   #. Observe the newly created `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ that are part of
      the new cluster.
   
   
   
   
   #. Wait for the machine to be created and automatically connect and start
      processing its own pipeline. This may take several minutes depending on
      the cloud.
   
      Troubleshooting note: if the machine does not join automatically, you
      can run the ```broker-start-agents-via-ansible-joinup``
      Blueprint <https://portal.rackn.io/#/e/0.0.0.0/blueprints/broker-start-agents-via-ansible-joinup/resources>`__
      from the resource broker to try to start the join-up via Ansible.
   


Resize the Cluster
++++++++++++++++++
   
   
   
   #. From your `Cluster <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1020>`__ after the Pipeline
      completes
   
   
   
   
   #. Note that the Cluster is now in “Work Order” mode.
   
   
   
   
   #. Look for the “``terraform/tfinfo``” to review the Terraform state file
      that will be used when updating or removing the cluster.
   
   
   
   
   #. Note the cost calculation in ``inventory/cost``
   
      The ``inventory/cost`` param has been updated as a total of all the
      ``inventory/cost`` values on the cluster’s machines.
   
   
   
   
   #. Change the ``cluster/count`` value.
   
      If zero, then all machines will be removed but the cluster will remain
      available for further action.
   
   
   
   
   #. Apply the ``universal-application-base-cluster`` Blueprint. This will
      automatically re-apply Terraform and update your cluster.
   
   
   
   
   #. From your Cloud Service Provider’s control panel, look to see the
      created instances.
   
   
   
   
   #. Note the updated cost calculation in ``inventory/cost``
   
      This value is automatically updated during cluster provisioning
   


Cleanup your cluster
++++++++++++++++++++

   This cluster is used in `Lab 1030 <https://portal.rackn.io/#/e/0.0.0.0/labs/rackn/1030>`__, so skip
   this step if you are continuing.
   
   
   
   #. From the `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__ table, select
      `lab1020 <https://portal.rackn.io/#/e/0.0.0.0/clusters/Name:lab1020>`__
   
   
   
   
   #. Use the Actions list to return your Cluster to ``Workflow mode``
   
   
   
   
   #. Use the Actions list to Cleanup your cluster (the related machines will
      be automatically removed also)
   
      Cleanup is a special version of Destroy that will run *and complete* the
      ``on-delete-workflow`` before invoking Destroy.
   
   
   
   
   #. From your Cloud Service Provider’s control panel, look to see that all
      instances were terminated.
   



