.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Leveraging Labs; Labs

.. _rs_cp_1001:

1001 Leveraging Labs
~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 1001
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: introductory
* Tags: labs
* Concepts: labs, UX

Objective
---------

Reviewing the Labs system to leverage all its features


  **Business ROI** Labs bring advanced Infrastructure as Code concepts
  into reach through narrative lessons.


Prerequisites
-------------
Required Labs:

* 1000

Addtional Checklist Items:

* A DRP endpoint


Summary
=======

.. tabs::

  .. tab:: Using Labs

  
     
     
     
     #. Navigate to the `Labs <https://portal.rackn.io/#/e/0.0.0.0/labs>`__ view from your UX by clicking the
        green Labs button in the top right
     
        **If you have already navigated to the Labs view, you can skip this step
        and the following one.**
     
     
     
     
     #. Open the lab ``1001`` **Leveraging Labs** by clicking it on this view
     
     #. Guided Lab Modal
        
        #. Pop out a guided lab modal by hovering over this step and clicking the
           blue pin to the right of it
        
           This will open a small modal that acts as a slideshow for the steps of
           the lab. It will persist when you navigate to different views in the UX
           or when you refresh your browser until you close the modal.
        
        
        #. Navigate to the next step by clicking the right arrow in the upper right
           corner of the modal
        
           Use this process to cycle through steps when using the guided lab modal.
        
        
        #. Navigate to another view in the UX, like the
           `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__ view
        
           Notice that the modal stays in place and keeps its step. Links can be
           clicked from the lab viewer or the guided lab modal, and the modal will
           keep in place or adjust accordingly.
        
        
        #. Toggle the guided lab modal into List View by clicking the list icon in
           the top right
        
           This mode displays all steps in the lab rather than one at a time. Steps
           can be popped out from this mode and the modal will switch to the popped
           out step just as it would from the viewer.
        
        
        #. Navigate back to this lab’s viewer by clicking the flask icon from the
           top right of the modal
        
        
        #. Select one of the following choices from either the modal or the viewer
        
        .. tabs::
        
          .. tab:: Choice A
        
          
             
             #. Proceed to the next step
             
                You selected Choice A, a tab. Tab choices for labs synchronize across
                the UX, so if you select a tab for an operating system or other choice
                once in a lab, your choice is remembered and reflected in any open
                modals or viewers.
             
          
        
          .. tab:: Choice B
        
          
             
             #. Proceed to the next step
             
                You selected Choice B, a tab. Tab choices for labs synchronize across
                the UX, so if you select a tab for an operating system or other choice
                once in a lab, your choice is remembered and reflected in any open
                modals or viewers.
             
          
        
     
     
     #. Lab Tree
        
        #. Explore the lab tree from the `Labs <https://portal.rackn.io/#/e/0.0.0.0/labs>`__ view
        
           Labs are organized top-down, so labs further up in the tree are depended
           on by labs further down in the tree. Connections between lab nodes
           depict the relationships between labs.
           
           For example, the lab `Detect Terraform Drift <https://portal.rackn.io/#/e/0.0.0.0/labs/rackn/1030>`__
           is dependent on the lab `Build a Multi-Cloud Cluster using Pre-Defined
           Terraform <https://portal.rackn.io/#/e/0.0.0.0/labs/rackn/1020>`__, as per the connection between the
           two nodes.
        
        
        #. Select the `Leveraging Labs <https://portal.rackn.io/#/e/0.0.0.0/labs/rackn/1001>`__ lab from the Labs
           view by clicking on its node in the tree
        
     
     
     #. Lab Viewer
        
        #. Explore the lab viewer.
        
           The lab viewer is the view that provides the most detail about a lab. It
           includes the lab’s tags, concepts, objective, description,
           prerequisites, video, and more
        
        
        #. Mark the lab as complete using the blue button in the top right of the
           viewer
        
           A lab can be marked as complete using this button. It will only mark as
           complete for your current browser, so other users of your DRP endpoint
           will not be affected by your lab completion.
           
           This will also cause the lab to gray out in the lab tree.
        
        
        #. Unmark the lab as complete by clicking the button again
        
     
     
     #. Lab Videos
        
        #. From the `Labs <https://portal.rackn.io/#/e/0.0.0.0/labs>`__ view, navigate to a lab with a video, such
           as `Manage Groups of Machines using Clusters <https://portal.rackn.io/#/e/0.0.0.0/labs/rackn/1010>`__
        
        
        #. Play the video by clicking it
        
        
        #. At any point, click the **Pop out video** button in the top right of the
           video to pop it out into a modal
        
           Your progress in the video will persist between the viewer and the
           modal.
           
           The video modal will follow you similarly to the guided lab modal. It
           can be closed at any time.
        
     
        
        #. Open the modal at this step if it is not still opened, and proceed to
           the next step
        
           The final step of a lab will allow you to mark it as complete, and view
           other labs that depend on the current lab. Otherwise, you can navigate
           back to the `lab tree <https://portal.rackn.io/#/e/0.0.0.0/labs>`__ to find more labs to work through.
        
     
  

  .. tab:: Disabling Labs

  
  
     This tab contains information for disabling the labs system. If you are
     here to learn how to use labs, please refer to the **Using Labs** tab.
     
     
     
     #. Navigate to the `global <https://portal.rackn.io/#/e/0.0.0.0/profiles/global>`__ profile in the
        `Profiles <https://portal.rackn.io/#/e/0.0.0.0/profiles>`__ view
     
     
     
     
     #. Navigate to the global profile’s
        `Params <https://portal.rackn.io/#/e/0.0.0.0/profiles/global/params>`__ tab
        
        #. If the `lab_urls <https://portal.rackn.io/#/e/0.0.0.0/params/lab_urls>`__ param appears, skip the
           following step. Otherwise, proceed to the next step
        
        
        #. Add it by clicking **Add Params** and typing in ``lab_urls``
        
           The param will be created with its default, an array containing only the
           URL to the RackN-provided labs.
        
     
     
     
     
     #. Disable labs by setting the param to an empty array
     
        Set the param value to the following.
        
        ::
        
           []
        
        Be sure to save your changes afterward.
     
     
     
     
     #. Refer to the `GitLab repository <https://gitlab.com/rackn/labs>`__
        README for information on adding, editing, and removing labs
     
        The README contains information on how to add your own labs, edit
        existing labs, remove labs, and create your own lab content packs.
     
  


