.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Override Cluster and Machine Pipelines; Labs

.. _rs_cp_2060:

2060 Override Cluster and Machine Pipelines
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 2060
* Time: 20 Minutes
* Enabled: Yes
* Difficulty: intermediate
* Tags: core, iac, pipelines
* Concepts: clusters, pipelines, params

Objective
---------

Create application specific pipeline overrides based on standardized
pipelines


  In this lab, the User will create a *Pipeline* and use it to build
  clusters.
  
  **Business ROI**: Using standardized pipelines helps ensure consistent
  operational results without restricting business imperatives for
  use-case specific needs.


Prerequisites
-------------
Required Labs:

* 1020



Summary
=======

Add the Dev Library Content Pack
++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to the `Catalog <https://portal.rackn.io/#/e/0.0.0.0/catalog>`__
   
   
   
   
   #. Find the ```dev-library`` <https://portal.rackn.io/#/e/0.0.0.0/catalog/dev-library>`__ using the search
      bar
   
   
   
   
   #. Install or Update ``dev-library`` if not loaded, by clicking the install
      button
   


Create a Pipelines
++++++++++++++++++
   
   
   
   #. Navigate to `Pipelines <https://portal.rackn.io/#/e/0.0.0.0/pipelines>`__
   
   
   
   
   #. Click the ``Create`` button
   
      The form should have the following:
      
      -  Set ``Name`` to ``lab2060``
      -  Set the Param ``universal-application`` to ``lab2060``
      
         -  The value must be the part after ``universal-application-`` in the
            ``Name`` field.
      
      -  Set the Param ``universal-application-chain-index-override`` to
         ``runbook``
      
         -  This will set the pipeline to apply a configurable application as
            the last step of the pipeline.
      
      -  Set the ``icon`` and ``color`` fields to your choice.
      -  Click ``Save``
   
   
   
   
   #. Click ``Add Params``
   
   
   
   
   #. Select ``dev/wait-icons`` by typing
   
   
   
   
   #. Select ``universal/runbook-during-flexiflow`` by typing
   
   
   
   
   #. Edit the ``dev/wait-icons`` parameter
   
      Make the list of values:
      
      .. code:: json
      
           [
             "cat",
             "arrow up",
             "arrow right",
             "arrow down",
             "arrow left",
             "dog"
           ]
      
      Click the little ``save`` icon
   
   
   
   
   #. Edit the ``universal/runbook-during-flexiflow`` parameter
   
      Make the values:
      
      .. code:: json
      
           [ "wait-time" ]
      
      Click the little ``save`` icon
      
      This injects the ``wait-time`` task into the runbook workflow element of
      the pipeline.
   


Look at the Pipeline Object
+++++++++++++++++++++++++++
   
   
   
   #. Review the `Runbook Workflow
      Entrypoints <https://portal.rackn.io/#/e/0.0.0.0/pipelines/universal-application-lab2060/chain>`__
   
      This view shows the Workflows chains in the Pipeline.
      
      Containers and VMs will enter through ``universal-start`` Workflow.
      
      Bare Metal and PXEing VMs will use the ``universal-discover`` Workflow.
   
   
   
   
   #. Review the `JSON <https://portal.rackn.io/#/e/0.0.0.0/pipelines/universal-application-lab2060/api>`__
   
      This view shows the raw JSON object for the Pipeline. The view can also
      show YAML.
      
      A button in the upper right, ``Enable Editing``, allows for direct
      editing of the object.
   


Create a Cluster to validate the Pipeline
+++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Click ``Add``
   
      Set ``Name`` to ``lab2060``
      
      Set the ``broker/name`` to ``context-broker``
      
      Set the ``broker/set-pipeline`` to ``universal-application-lab2060``
      
      Click ``Save``
   


Review the Machines Created by the Cluster
++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
   
   
   
   #. Notice the Machines with names starting with ``lab2060-``
   
      The machines were created by the ``context-broker`` for the ``lab2060``
      clusters.
      
      These machines were assigned the ``universal-application-lab2060``
      pipeline and are running that now. The icons of the machine should be
      cycling through the list in the pipeline.
      
      The task is set to ``wait-time``.
   


Clean up the cluster
++++++++++++++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Select the ``lab2060`` cluster
   
   
   
   
   #. Click ``Actions`` and select ``Workflow Mode``
   
      Clusters must be ``Workflow Modee`` to be removed.
   
   
   
   
   #. Click ``Actions`` and select ``Delete (Cleanup)``
   
   
   
   
   #. Click ``Cleanup`` to confirm
   
      The cluster will run a workflow to cleanup the machines by asking the
      broker to remove them. Once complete, the cluster will be removed.
   


Create Cluster Pipeline
+++++++++++++++++++++++
   
   
   
   #. Navigate to `Pipelines <https://portal.rackn.io/#/e/0.0.0.0/pipelines>`__
   
   
   
   
   #. Click the ``Create`` button
   
      The form should have the following:
      
      -  Set ``Name`` to ``lab2060-cluster``
      -  Set the Param ``universal-application`` to ``lab2060-cluster``
      
         -  The value must be the part after ``universal-application-`` in the
            ``Name`` field.
      
      -  Set the Param ``universal-application-chain-index-override`` to
         ``cluster``
      
         -  This will set the pipeline to follow set of workflows to create a
            cluster.
      
      -  Set the Meta required params field to ``broker/name``
      
         -  This will require that the cluster page ask for that field. This
            is a space-separated list.
      
      -  Click ``Save``
   
   
   
   
   #. Click ``Add Params``
   
   
   
   
   #. Select ``broker/set-pipeline`` by typing
   
   
   
   
   #. Select ``universal/runbook-during-flexiflow`` by typing
   
   
   
   
   #. Edit the ``broker/set-pipeline`` parameter
   
      The value should be set to ``universal-application-lab2060``.
      
      Click the little ``save`` icon
   
   
   
   
   #. Edit the ``universal/runbook-during-flexiflow`` parameter
   
      Make the values:
      
      .. code:: json
      
           [ "wait-time" ]
      
      Click the little ``save`` icon
      
      This injects the ``wait-time`` task into the runbook workflow element of
      the pipeline.
   


Create a Cluster to validate the Cluster Pipeline
+++++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Click ``Add``
   
      Set ``Name`` to ``lab2060``
      
      Set ``Cluster Pipeline`` to ``lab2060-cluster``
      
      **NOTICE** The required parameters changed.
      
      Set the ``broker/name`` to ``context-broker``
      
      Do NOT set the ``broker/set-pipeline`` parameter
      
      Click ``Save``
   
   
   
   
   #. Navigate to the `lab2060 <https://portal.rackn.io/#/e/0.0.0.0/clusters/lab2060/activity>`__ Activity
      view
   
      Notice that this time the cluster continues on to running the
      ``wait-time`` task after creating the cluster machines.
   
   
   
   
   #. Navigate to `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
      If this is done quickly, the machines will not have necessarily
      completed all there initialization and pipeline completion. The Cluster
      pipeline defaults to not waiting for machines to complete their
      pipelines before the cluster continues. This can be changed.
   


Clean up the cluster
++++++++++++++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Select the ``lab2060`` cluster
   
   
   
   
   #. Click ``Actions`` and select ``Workflow Mode``
   
      Clusters must be ``Workflow Modee`` to be removed.
   
   
   
   
   #. Click ``Actions`` and select ``Delete (Cleanup)``
   
   
   
   
   #. Click ``Cleanup`` to confirm
   
      The cluster will run a workflow to cleanup the machines by asking the
      broker to remove them. Once complete, the cluster will be removed.
   


Update Cluster Pipeline to Wait for Machines
++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `lab2060-cluster <https://portal.rackn.io/#/e/0.0.0.0/pipelines/lab2060-cluster>`__
      Pipeline
   
   
   
   
   #. Click ``Add Params``
   
   
   
   
   #. Select ``universal/cluster-provision-post-flexiflow`` by typing
   
   
   
   
   #. Edit the ``universal/cluster-provision-post-flexiflow`` parameter
   
      Make the values:
      
      .. code:: json
      
           [ "cluster-wait-for-members" ]
      
      Click the little ``save`` icon
      
      This injects the ``cluster-wait-for-members`` task into the cluster
      provision workflow element of the pipeline.
      
      This shows injection of tasks. Alternatively, there is a parameter for
      this already, ``cluster/wait-for-members``. Setting this to ``true``
      instead of this will cause the same effect.
   


Create a Cluster to validate the Cluster Pipeline
+++++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Click ``Add``
   
      Set ``Name`` to ``lab2060``
      
      Set ``Cluster Pipeline`` to ``lab2060-cluster``
      
      **NOTICE** The required parameters changed.
      
      Set the ``broker/name`` to ``context-broker``
      
      Do NOT set the ``broker/set-pipeline`` parameter
      
      Click ``Save``
   
   
   
   
   #. Navigate to the `lab2060 <https://portal.rackn.io/#/e/0.0.0.0/clusters/lab2060/activity>`__ Activity
      view
   
      Notice that this time the cluster runs ``cluster-wait-for-members`` and
      pauses waiting for the machines to finish.
   
   
   
   
   #. Navigate to `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
      If this is done quickly, the machines will not have necessarily
      completed all there initialization and pipeline completion. Once all the
      machines finish, the cluster will then run the ``wait-time`` task.
   


Clean up the cluster
++++++++++++++++++++
   
   
   
   #. Navigate to `Clusters <https://portal.rackn.io/#/e/0.0.0.0/clusters>`__
   
   
   
   
   #. Select the ``lab2060`` cluster
   
   
   
   
   #. Click ``Actions`` and select ``Workflow Mode``
   
      Clusters must be ``Workflow Modee`` to be removed.
   
   
   
   
   #. Click ``Actions`` and select ``Delete (Cleanup)``
   
   
   
   
   #. Click ``Cleanup`` to confirm
   
      The cluster will run a workflow to cleanup the machines by asking the
      broker to remove them. Once complete, the cluster will be removed.
   



