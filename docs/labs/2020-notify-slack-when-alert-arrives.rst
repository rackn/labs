.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Notify Slack when Alerts Arrive; Labs

.. _rs_cp_2020:

2020 Notify Slack when Alerts Arrive
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 2020
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: intermediate
* Tags: slack, orchestration, integration, triggers
* Concepts: alerts, triggers, blueprints
..  youtube:: VBnGduQdrnE
    :privacy_mode:
`Video Link <https://youtu.be/VBnGduQdrnE>`__

Objective
---------

Integrate Digital Rebar Provision events and alerts into Slack
notifications


  In this lab, the User will explore how to orchestrate the delivery of
  Slack notifications on events and alerts.
  
  **Business ROI**: Improved visibility into operational status improves
  system uptime and maintenance


Prerequisites
-------------
Required Labs:

* 2000
* 2010

Addtional Checklist Items:

* Must have authority to create a webhook application in Slack
  `see <https://api.slack.com/messaging/sending>`__
* A Slack Messaging Application URL

    Per the `Slack Documentation <https://hooks.slack.com/services/>`__, the
    URL will need appropriate tokens to access slack.


Notify Slack when Alerts Arrive
===============================

Build orchestration
+++++++++++++++++++

   Choose a path to setup the orchestration. Both paths can be done without
   resetting anything.

.. tabs::

  .. tab:: UX

  
  
  #. Setup Slack Service Integration
     
     #. Navigate to the `global <https://portal.rackn.io/#/e/0.0.0.0/profiles/global>`__ profile
     
     
     #. Add Parameter by clicking ``Add Params``
     
        Search and select ``slack/service-url``
        
        Click ``Decrypt Secure Param``
        
        Enter the URL for Slack Messaging Application URL
        
        Click outside of the text box to save the value
     
  
  
  #. Setup a Blueprint
     
     #. Navigate to the `Blueprints <https://portal.rackn.io/#/e/0.0.0.0/blueprints>`__ view
     
     
     #. Click the ``Add`` button
     
        Set ``Name`` to ``lab2020``
        
        Click ``Save``
     
     
     #. Add a Task by selecting ``slack-app-webhook``
     
  
  
  #. Create a Trigger to Run the Blueprint
     
     #. Go to the `Triggers <https://portal.rackn.io/#/e/0.0.0.0/triggers>`__ view
     
     
     #. Click the ``Add`` button
     
        Set ``Name`` to ``lab2020``
        
        Set ``Provider`` to ``event-trigger``
        
        Set ``Blueprint`` to ``lab2020``
        
        Set ``Filter`` to ``Local Self Runner``
        
        Add a Param ``event-trigger/event-match`` with a value of
        ``alerts.create.*``
        
        Click ``Save``
     
  
  
  #. Create an Alert to Test the Process
     
     #. Go to the `Alerts <https://portal.rackn.io/#/e/0.0.0.0/alerts>`__ view
     
     
     #. Click the ``Add`` button
     
        Set ``Name`` to ``lab2020``
        
        Set ``Level`` to ``INFO``
        
        Click ``Save``
     
     
     #. Check Slack for a message from that alert.
     
  
  
  #. Observe Work Orders
     
     #. Go to `Work Orders <https://portal.rackn.io/#/e/0.0.0.0/work_orders>`__ view
     
     
     #. Notice the ``lab2020`` Blueprint WorkOrder
     
  
  
  #. Customize the message
     
     #. Go to the `lab2020 <https://portal.rackn.io/#/e/0.0.0.0/triggers/lab2020>`__ trigger
     
     
     #. Click the ``Merge Data`` toggle to ``true``
     
     
     #. Add a WorkOrder Parameter ``slack/message``
     
        The value should be set to:
        
        .. code:: json
        
           {"text":"{{.Param "Level"}} {{.Param "Name"}} Alert from Digital Rebar at {{.ApiURL }}"}
     
  
  
  #. Create an Alert to Test the Custom Message
     
     #. Go to the `Alerts <https://portal.rackn.io/#/e/0.0.0.0/alerts>`__ view
     
     
     #. Click the ``Add`` button
     
        Set ``Name`` to ``lab2020``
        
        Set ``Level`` to ``INFO``
        
        Click ``Save``
     
     
     #. Check Slack for a message from that alert.
     
  
  

  .. tab:: CLI

  
  
  #. Using the Shell from `Lab 2000 <https://portal.rackn.io/#/e/0.0.0.0/labs/rackn/2000>`__
     
     #. Bring up the shell
     
  
  
  #. Setup Slack Service Integration
     
     #. Set the Slack URL in the global profile
     
        .. code:: sh
        
           drpcli profiles set global param slack/service-url to <URL from Prereqs>
        
        Returns the URL on success. It will automatically be encrypted on the
        server as a secure parameter.
     
  
  
  #. Setup a Blueprint
     
     #. Create a blueprint to do slack notifications
     
        .. code:: sh
        
           drpcli blueprints create - <<EOF
           Name: lab2020-cli
           Tasks:
             - slack-app-webhook
           EOF
        
        Returns the JSON object of the new blueprint on success.
     
  
  
  #. Create a Trigger to Run the Blueprint
     
     #. Create a trigger to run the blueprint
     
        .. code:: sh
        
           drpcli triggers create - <<EOF
           Name: lab2020-cli
           Blueprint: lab2020-cli
           Description: 'Notify Slack on new Alerts'
           Enabled: true
           Filter: Params.machine-self-runner=true Endpoint=
           FilterCount: 1
           MergeDataIntoParams: true
           Params:
             event-trigger/event-match: alerts.create.*
           TriggerProvider: event-trigger
           WorkOrderParams:
             slack/message: >-
               {"text":"{{.Param "Level"}} {{.Param "Name"}} Alert from Digital Rebar at {{.ApiURL }}"}
           EOF
        
        Returns the JSON object of the new trigger on success.
     
  
  
  #. Create an Alert to Test the Process
     
     #. Post the alert
     
        .. code:: sh
        
           drpcli alerts post --unique INFO lab2020-cli
        
        Returns the JSON object of the alert.
     
  
  
  #. Observe Work Orders
     
     #. Go to `Work Orders <https://portal.rackn.io/#/e/0.0.0.0/work_orders>`__ view
     
     
     #. Notice the ``lab2020-cli`` Blueprint WorkOrder
     
  
  



