.. Copyright (c) 2022 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Create Your Own Blueprint to Repond to Triggers; Labs

.. _rs_cp_3010:

3010 Create Your Own Blueprint to Repond to Triggers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview
--------

* Id: 3010
* Time: 10 Minutes
* Enabled: Yes
* Difficulty: advanced
* Tags: core
* Concepts: blueprints, triggers, events

Objective
---------

Create reusable automation bundles that can be applied to user, event,
or time-based activity from the existing task library


  In this lab, the User will create, modify, and run a Blueprint.
  
  **Business ROI**: Consistent care and feeding of key infrastructure that
  can be shared, validated, and audited


Prerequisites
-------------
Required Labs:

* 1005



Summary
=======

Add the Dev Library Content Pack
++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to the `Catalog <https://portal.rackn.io/#/e/0.0.0.0/catalog>`__
   
   
   
   
   #. Find the ```dev-library`` <https://portal.rackn.io/#/e/0.0.0.0/catalog/dev-library>`__ using the search
      bar
   
   
   
   
   #. Install or Update ``dev-library`` if not loaded, by clicking the green
      download button
   


Create a Blueprint
++++++++++++++++++
   
   
   
   #. Navigate to `Blueprints <https://portal.rackn.io/#/e/0.0.0.0/blueprints>`__
   
   
   
   
   #. Click ``Create``
   
      Set ``Name`` to ``lab3010``
      
      Click ``Continue``
   
   
   
   
   #. Click ``Add Task``, then search for and add ``wait-time``
   
   
   
   
   #. Navigate to the `Activity Tab <https://portal.rackn.io/#/e/0.0.0.0/blueprints/lab3010>`__
   
   
   
   
   #. Select the DRP Machine in the table
   
   
   
   
   #. Click ``Apply lab3010``
   
   
   
   
   #. Watch the icon on the self runner cycle
   


Create a Trigger to run the Blueprint
+++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Triggers <https://portal.rackn.io/#/e/0.0.0.0/triggers>`__
   
   
   
   
   #. Click ``Create``
   
      Set ``Name`` to ``lab3010``
      
      Set ``Trigger Provider`` to ``event-trigger``
      
      Set ``Blueprint`` to ``lab3010``
      
      Set ``Filter`` to ``Local Self Runner`` from the ``Common Filters``
      dropdown
      
      Set ``event-trigger/event-match`` parameter to ``ux.button.lab3010``
      
      Click ``Save``
      
      Refresh the browser to make the button appear in the Trigger Buttons
      list
   


Invoke the Trigger / Blueprint Pair
+++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Work Orders <https://portal.rackn.io/#/e/0.0.0.0/work_orders>`__
   
   
   
   
   #. Click the ``lab3010`` trigger button in the upper right button list
   
   
   
   
   #. Observe a Work Order being created and run by the self-runner
   
   
   
   
   #. Click the ``uuid`` of the Work Order
   
   
   
   
   #. Watch the task run
   


Define Parameters on the Blueprint
++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to the `lab3010 <https://portal.rackn.io/#/e/0.0.0.0/blueprints/lab3010/params>`__ Blueprint
      Params
   
   
   
   
   #. Click ``Add Params`` and select ``dev/wait-icons`` and ``dev/wait-time``
   
   
   
   
   #. Edit ``dev/wait-icons``
   
      Set the value to:
      
      .. code:: json
      
         ["heart", "lab", "cat", "dog"]
      
      Click the little ``save`` icon
   
   
   
   
   #. Edit ``dev/wait-time``
   
      Set the value to 10
      
      Click the little ``save`` icon
      
      This is the number of seconds this task is going to run.
   


Invoke the Trigger / Blueprint Pair to See the Changes to the Blueprint
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   
   
   
   #. Navigate to `Machines <https://portal.rackn.io/#/e/0.0.0.0/machines>`__
   
   
   
   
   #. Click the ``lab3010`` trigger button in the upper right button list
   
   
   
   
   #. Observer the icon on the DRP Endpoint machine cycle
   
   
   
   
   #. Watch the task run
   



